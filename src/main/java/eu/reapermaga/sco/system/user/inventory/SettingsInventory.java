package eu.reapermaga.sco.system.user.inventory;

import eu.reapermaga.sco.system.ScoPlugin;
import eu.reapermaga.sco.system.user.SpigotUser;
import eu.reapermaga.sco.system.user.obj.Settings;
import eu.reapermaga.sco.system.util.inventory.InventoryBuilder;
import eu.reapermaga.sco.system.util.inventory.buttons.InventoryButton;
import eu.reapermaga.sco.system.util.inventory.buttons.impl.ClickButton;
import eu.reapermaga.sco.system.util.inventory.util.ItemCreator;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;

/**
 * Copyright (c) ReaperMaga, All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by ReaperMaga
 **/
public class SettingsInventory extends InventoryBuilder {

    private static final ItemStack ON_ITEM = new ItemCreator().setMaterial(Material.INK_SACK).setData(10).setName("§aOn").build();
    private static final ItemStack OFF_ITEM = new ItemCreator().setMaterial(Material.INK_SACK).setData(8).setName("§cOff").build();

    public SettingsInventory(Player player) {
        super(ScoPlugin.getInstance(), player, "§cSettings", 2);
        addContent();
        build();
    }

    private void addContent() {
        SpigotUser user = SpigotUser.getSpigotUser(getForPlayer());

        int slot = 0;
        for(Settings settings : Settings.values()) {
            addEmptyButton(slot, settings.getIcon());


            addButton(new InventoryButton((slot + 9), (user.isSetting(settings) ? ON_ITEM : OFF_ITEM), new ClickButton() {
                @Override
                public void onClick(Player player, InventoryClickEvent event) {
                    boolean currentValue = user.isSetting(settings);
                    updateButton(event.getRawSlot(), (currentValue ? OFF_ITEM : ON_ITEM));
                    user.playSound(Sound.BLOCK_WOOD_BUTTON_CLICK_ON);

                    currentValue = currentValue ? false : true;
                    user.setSetting(settings, currentValue);

                    /**
                     * Specific
                     */
                }
            }));


            if(slot == 9){
                slot = 18;
            } else {
                slot++;
            }


        }
    }
}
