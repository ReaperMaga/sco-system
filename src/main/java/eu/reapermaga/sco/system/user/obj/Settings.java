package eu.reapermaga.sco.system.user.obj;

import eu.reapermaga.sco.system.util.inventory.util.ItemCreator;
import lombok.AllArgsConstructor;
import lombok.Getter;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

/**
 * Copyright (c) ReaperMaga, All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by ReaperMaga
 **/
@Getter
@AllArgsConstructor
public enum Settings {

    PRIVATE_MESSAGES(true, new ItemCreator().setMaterial(Material.BOOK_AND_QUILL).setName("§cPrivate messages").build()),
    CLEARLAG_NOTIFICATIONS(true, new ItemCreator().setMaterial(Material.DEAD_BUSH).setName("§cClearlag notifications").build()),
    GRASS_REMOVE(true, new ItemCreator().setMaterial(Material.LONG_GRASS).setData(1).setName("§cClear Grass").build());


    private final boolean defaultValue;
    private final ItemStack icon;

}
