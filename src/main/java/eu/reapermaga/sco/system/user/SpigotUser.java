package eu.reapermaga.sco.system.user;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Queues;
import eu.reapermaga.sco.system.ScoPlugin;
import eu.reapermaga.sco.system.database.ext.DatabaseFilter;
import eu.reapermaga.sco.system.database.mongo.impl.user.UserData;
import eu.reapermaga.sco.system.database.mongo.impl.user.rep.UserRepository;
import eu.reapermaga.sco.system.file.ConfigurationFiles;
import eu.reapermaga.sco.system.file.json.GsonConfig;
import eu.reapermaga.sco.system.io.Message;
import eu.reapermaga.sco.system.user.obj.Settings;
import eu.reapermaga.sco.system.util.AsyncDispatcher;
import lombok.Getter;
import lombok.Setter;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;

import java.io.File;
import java.util.List;
import java.util.Map;
import java.util.Queue;

/**
 * Copyright (c) ReaperMaga, All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by ReaperMaga
 **/
@Getter
public class SpigotUser extends GsonConfig<UserData> {

    public static final List<SpigotUser> USERS = Lists.newCopyOnWriteArrayList();

    @Setter
    private Player player;
    private Map<String, Object> tags;

    public SpigotUser(Player player) {
        super(ConfigurationFiles.getDefaultPath() + "userdata" + File.separator, player.getUniqueId().toString());
        this.player = player;
        this.tags = Maps.newHashMap();
        loadData();
    }

    public UserData getUserData() {
        return getEntity();
    }



    @Override
    public UserData getDefault() {
        UserData data = new UserData();
        data.setName("None");
        data.setUuid("None");
        data.setSettings(Maps.newHashMap());
        return data;
    }

    public boolean isSetting(Settings settings){
        if(!getUserData().getSettings().containsKey(settings.name())){
            return settings.isDefaultValue();
        }
        return getUserData().getSettings().get(settings.name());
    }

    public void setSetting(Settings setting, boolean value){
        getUserData().getSettings().put(setting.name(), value);
    }

    private void loadData() {
        getEntity().setUuid(player.getUniqueId().toString());
        getEntity().setName(player.getName());
        save();
    }

    public void saveData() {
        AsyncDispatcher.dispatch(() -> {
            save();
        });
    }


    public boolean containsGrassBlock(Block block) {
        for(Block check : getQueueGrass()) {
            if(check.getLocation().getBlockX() == block.getLocation().getBlockX()
            && check.getLocation().getBlockY() == block.getLocation().getBlockY() &&
            check.getLocation().getBlockZ() == block.getLocation().getBlockZ()) {
                return true;
            }
        }
        return false;
    }

    public void addGrass(Block block) {
        Queue<Block> queue = getQueueGrass();
        queue.add(block);
    }

    public Block pollGrass() {
        Queue<Block> queue = getQueueGrass();
        Block block = queue.poll();
        return block;
    }

    public Queue<Block> getQueueGrass() {
        if(!containsTag("grass_respawn")) {
            setTag("grass_respawn", Queues.newLinkedBlockingQueue());
        }
        return getTagValue("grass_respawn");
    }

    public boolean containsTag(String key) {
        return this.tags.containsKey(key);
    }

    public void setTag(String key, Object value) {
        this.tags.put(key, value);
    }

    public <T> T getTagValue(String key) {
        return (T) this.tags.get(key);
    }


    public void removeTag(String key) {
        if (containsTag(key)) {
            this.tags.remove(key);
        }
    }



    public void playSound(Sound sound) {
        player.playSound(player.getLocation(), sound, 1f, 1f);
    }

    public void teleport(Location location, boolean withSound) {
        player.teleport(location);
        if (withSound) {
            playSound(Sound.ENTITY_ENDERMEN_TELEPORT);
        }
    }



    public void sendMessage(String message) {
        if(player.isOnline()){
            player.sendMessage(Message.PREFIX + message);
        }
    }

    public static SpigotUser getSpigotUser(Player player) {
        for (SpigotUser user : USERS) {
            if (user.getPlayer().getName().equals(player.getName())) {
                user.setPlayer(player);
                return user;
            }
        }
        SpigotUser user = new SpigotUser(player);
        USERS.add(user);
        return user;
    }

    public static void saveOnlineUsers() {
        for (Player all : Bukkit.getOnlinePlayers()) {
            SpigotUser user = SpigotUser.getSpigotUser(all);
            user.saveData();
        }
    }

    public static void registerOnlineUsers() {
        for (Player all : Bukkit.getOnlinePlayers()) {
            SpigotUser user = SpigotUser.getSpigotUser(all);
            AsyncDispatcher.dispatch(()-> {
                user.loadData();

            });
        }
    }



}
