package eu.reapermaga.sco.system.database.ext;

import lombok.Getter;
import lombok.Setter;

/**
 * Copyright (c) ReaperMaga, All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by ReaperMaga
 **/
@Getter
@Setter
public class Credentials {

    private boolean local;
    private String host;
    private int port;
    private String database;
    private String user;
    private String password;

    public Credentials(String database) {
        this.local = true;
        this.database = database;
    }

    public Credentials() {
        this.local = true;
    }

}
