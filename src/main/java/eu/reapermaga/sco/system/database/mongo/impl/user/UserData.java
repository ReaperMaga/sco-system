package eu.reapermaga.sco.system.database.mongo.impl.user;

import eu.reapermaga.sco.system.user.obj.Settings;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Map;

/**
 * Copyright (c) ReaperMaga, All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by ReaperMaga
 **/
@NoArgsConstructor
@Getter
@Setter
public class UserData {
    
    private String uuid;
    private String name;
    private Map<String, Boolean> settings;


    public boolean isSetting(Settings settings){
        if(!this.settings.containsKey(settings.name())){
            return settings.isDefaultValue();
        }
        return this.settings.get(settings.name());
    }

}
