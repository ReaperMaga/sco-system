package eu.reapermaga.sco.system.database.mongo.repository.mongo;


import com.google.common.collect.Lists;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.model.Filters;
import eu.reapermaga.sco.system.database.ext.DatabaseFilter;
import eu.reapermaga.sco.system.database.mongo.MongoContext;
import eu.reapermaga.sco.system.database.mongo.repository.CrudRepository;
import lombok.Getter;
import org.bson.Document;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.List;

/**
 * Copyright (c) ReaperMaga, All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by ReaperMaga
 **/
@Getter
public abstract class AbstractMongoCrudRepository<T, ID> implements CrudRepository<T, ID> {

    private volatile Class<T> entityClass;

    private MongoContext mongoContext;
    private MongoCollection<T> collection;

    protected AbstractMongoCrudRepository(MongoContext mongoContext) {
        this.mongoContext = mongoContext;
        this.collection = mongoContext.getMongoDatabase().getCollection(getCollectionName(), getEntityClass());
    }

    public void create(T entity) {
        this.collection.insertOne(entity);
    }

    public T findById(DatabaseFilter filter, ID id) {
        return collection.find(Filters.eq(filter.getKey(), id)).first();
    }

    public List<T> findAll() {
        List<T> list = Lists.newLinkedList();
        FindIterable<T> findIterable = collection.find();
        for(T value : findIterable){
            list.add(value);
        }
        return list;
    }

    public void update(DatabaseFilter filter, ID id, T entity) {
        collection.updateOne(Filters.eq(filter.getKey(), id), new Document("$set", entity));

    }

    public void delete(T entity) {

    }

    public void deleteById(DatabaseFilter filter,ID id) {
        collection.deleteOne(Filters.eq(filter.getKey(), id));
    }

    public boolean existsById(DatabaseFilter filter, ID id) {
        FindIterable<T> findIterable = collection.find(Filters.eq(filter.getKey(), id));
        return findIterable.first() != null;
    }

    public abstract String getCollectionName();



    protected Class<T> getEntityClass() {
        Class<T> tmp = entityClass;
        if (tmp == null) {
            synchronized (this) {
                tmp = entityClass;
                if (tmp == null) {
                    entityClass = tmp =  (Class<T>) getGenericType(getClass(), 0);
                }
            }
        }

        return tmp;
    }

    protected Class<?> getGenericType(Class<?> clazz, int typeIndex) {

        Type[] types = ((ParameterizedType) clazz.getGenericSuperclass()).getActualTypeArguments();

        return (Class<?>) types[typeIndex];
    }


}
