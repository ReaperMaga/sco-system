package eu.reapermaga.sco.system.database.ext;

/**
 * Copyright (c) ReaperMaga, All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by ReaperMaga
 **/
public enum DatabaseFilter {

    UUID,
    NAME;

    public String getKey(){
        return name().toLowerCase();
    }
}

