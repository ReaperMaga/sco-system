package eu.reapermaga.sco.system.database.mongo.impl.user.rep;


import eu.reapermaga.sco.system.database.ext.DatabaseFilter;
import eu.reapermaga.sco.system.database.mongo.MongoContext;
import eu.reapermaga.sco.system.database.mongo.impl.user.UserData;
import eu.reapermaga.sco.system.database.mongo.repository.mongo.AbstractMongoCrudRepository;

import java.util.List;

/**
 * Copyright (c) ReaperMaga, All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by ReaperMaga
 **/
public class MongoUserRepository extends AbstractMongoCrudRepository<UserData, String> implements UserRepository{

    public MongoUserRepository(MongoContext mongoContext) {
        super(mongoContext);
    }

    @Override
    public void saveData(UserData data) {
        update(DatabaseFilter.UUID, data.getUuid(), data);
    }

    @Override
    public UserData findByPlayerName(String name) {
        List<UserData> find = findAll();
        for(UserData data : find){
            if(data.getName().equalsIgnoreCase(name)){
                return data;
            }
        }
        return null;
    }

    @Override
    public void deleteByPlayerName(String name) {
        List<UserData> find = findAll();
        for(UserData data : find){
            if(data.getName().equalsIgnoreCase(name)){
                deleteById(DatabaseFilter.UUID, data.getUuid());
            }
        }
    }

    @Override
    public boolean existsByName(String name) {
        List<UserData> find = findAll();
        for(UserData data : find){
            if(data.getName().equalsIgnoreCase(name)){
                return true;
            }
        }
        return false;
    }

    @Override
    public String getCollectionName() {
        return "users";
    }
}
