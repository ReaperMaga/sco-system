package eu.reapermaga.sco.system.database.mongo.impl.user.rep;


import eu.reapermaga.sco.system.database.mongo.impl.user.UserData;
import eu.reapermaga.sco.system.database.mongo.repository.CrudRepository;

/**
 * Copyright (c) ReaperMaga, All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by ReaperMaga
 **/
public interface UserRepository extends CrudRepository<UserData, String> {

    UserData findByPlayerName(String name);

    void deleteByPlayerName(String name);

    boolean existsByName(String name);

    void saveData(UserData data);


}
