package eu.reapermaga.sco.system.database.mongo.repository;


import eu.reapermaga.sco.system.database.ext.DatabaseFilter;

import java.util.List;

/**
 * Copyright (c) ReaperMaga, All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by ReaperMaga
 **/
public interface CrudRepository<T, ID> {

    void create(T entity);

    T findById(DatabaseFilter filter, ID id);

    List<T> findAll();

    void update(DatabaseFilter filter, ID id, T entity);

    void delete(T entity);

    void deleteById(DatabaseFilter filter, ID id);

    boolean existsById(DatabaseFilter filter, ID id);


}
