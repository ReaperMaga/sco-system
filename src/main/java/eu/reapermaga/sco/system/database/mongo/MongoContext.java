package eu.reapermaga.sco.system.database.mongo;

import com.mongodb.MongoClient;
import com.mongodb.MongoClientOptions;
import com.mongodb.MongoCredential;
import com.mongodb.ServerAddress;
import com.mongodb.client.MongoDatabase;
import eu.reapermaga.sco.system.database.ext.Credentials;
import eu.reapermaga.sco.system.database.mongo.repository.mongo.MongoRepositoryHolder;
import lombok.Getter;
import org.bson.codecs.configuration.CodecRegistries;
import org.bson.codecs.configuration.CodecRegistry;
import org.bson.codecs.pojo.PojoCodecProvider;

/**
 * Copyright (c) ReaperMaga, All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by ReaperMaga
 **/
@Getter
public class MongoContext {

    private Credentials credentials;
    private MongoClient mongoClient;
    private MongoDatabase mongoDatabase;
    private MongoRepositoryHolder mongoRepositoryHolder;

    public MongoContext(Credentials credentials) {
        this.credentials = credentials;
        this.mongoRepositoryHolder = new MongoRepositoryHolder(this);
    }

    public void connect() {
        CodecRegistry pojoCodecRegistry = CodecRegistries.fromRegistries(MongoClient.getDefaultCodecRegistry(),
                CodecRegistries.fromProviders(PojoCodecProvider.builder().automatic(true).build()));
        if(credentials.isLocal()){
            this.mongoClient = new MongoClient(new ServerAddress("localhost", 27017), MongoClientOptions.builder().codecRegistry(pojoCodecRegistry).build());
        } else {
            MongoCredential mongoCredential = MongoCredential.createCredential(credentials.getUser(), credentials.getDatabase(), credentials.getPassword().toCharArray());
            MongoClientOptions options =  MongoClientOptions.builder().codecRegistry(pojoCodecRegistry).build();
           this.mongoClient = new MongoClient(new ServerAddress(credentials.getHost(), credentials.getPort()), mongoCredential, options);
        }
        this.mongoDatabase = this.mongoClient.getDatabase(credentials.getDatabase());
        this.mongoRepositoryHolder.register();

    }

    public void disconnect() {
        this.mongoClient.close();
    }

}
