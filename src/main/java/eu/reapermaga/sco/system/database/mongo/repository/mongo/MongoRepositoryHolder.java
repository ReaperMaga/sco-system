package eu.reapermaga.sco.system.database.mongo.repository.mongo;

import com.google.common.collect.Lists;
import eu.reapermaga.sco.system.database.mongo.MongoContext;
import eu.reapermaga.sco.system.database.mongo.impl.user.rep.MongoUserRepository;
import eu.reapermaga.sco.system.database.mongo.impl.user.rep.UserRepository;
import eu.reapermaga.sco.system.database.mongo.repository.CrudRepository;
import lombok.Getter;

import java.util.List;

/**
 * Copyright (c) ReaperMaga, All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by ReaperMaga
 **/
@Getter
public class MongoRepositoryHolder {

    private final MongoContext mongoContext;

    private List<CrudRepository<?, ?>> repositories = Lists.newLinkedList();

    private UserRepository userRepository;


    public MongoRepositoryHolder(MongoContext mongoContext) {
        this.mongoContext = mongoContext;
    }

    /**
     * Add repository
     *
     * @param repository
     */
    public void addRepository(CrudRepository<?, ?> repository) {
        getRepositories().add(repository);
    }


    public void register() {
        addRepository(userRepository = new MongoUserRepository(mongoContext));
    }
}
