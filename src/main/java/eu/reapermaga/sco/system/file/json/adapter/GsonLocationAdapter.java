package eu.reapermaga.sco.system.file.json.adapter;


import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import com.google.gson.stream.JsonWriter;
import org.bukkit.Bukkit;
import org.bukkit.Location;

import java.io.IOException;

/**
 * Copyright (c) ReaperMaga, All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by ReaperMaga
 **/
public class GsonLocationAdapter extends TypeAdapter<Location> {

    @Override
    public void write(JsonWriter jsonWriter, Location location) throws IOException {
        jsonWriter.beginObject();
        jsonWriter.name("world");
        jsonWriter.value(location.getWorld().getName());

        jsonWriter.name("x");
        jsonWriter.value(location.getX());

        jsonWriter.name("y");
        jsonWriter.value(location.getY());

        jsonWriter.name("z");
        jsonWriter.value(location.getZ());

        jsonWriter.name("yaw");
        jsonWriter.value(location.getYaw());

        jsonWriter.name("pitch");
        jsonWriter.value(location.getPitch());
        jsonWriter.endObject();
    }

    @Override
    public Location read(JsonReader jsonReader) throws IOException {
        jsonReader.beginObject();
        String worldName = "";
        double x = 0.0;
        double y = 0.0;
        double z = 0.0;
        double yaw = 0.0;
        double pitch = 0.0;
        String fieldName = "";
        while (jsonReader.hasNext()){
            JsonToken token = jsonReader.peek();
            if(token.equals(JsonToken.NAME)){
                fieldName = jsonReader.nextName();
            }
            if(fieldName.equals("world")){
                token = jsonReader.peek();
                worldName = jsonReader.nextString();
            }
            if(fieldName.equals("x")){
                token = jsonReader.peek();
                x = jsonReader.nextDouble();
            }
            if(fieldName.equals("y")){
                token = jsonReader.peek();
                y = jsonReader.nextDouble();
            }
            if(fieldName.equals("z")){
                token = jsonReader.peek();
                z = jsonReader.nextDouble();
            }
            if(fieldName.equals("yaw")){
                token = jsonReader.peek();
                yaw = jsonReader.nextDouble();
            }
            if(fieldName.equals("pitch")){
                token = jsonReader.peek();
                pitch = jsonReader.nextDouble();
            }
        }
        jsonReader.endObject();
        return new Location(Bukkit.getWorld(worldName), x, y, z, (float)yaw, (float)pitch);
    }
}
