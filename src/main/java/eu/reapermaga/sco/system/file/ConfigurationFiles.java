package eu.reapermaga.sco.system.file;

import eu.reapermaga.sco.system.ScoPlugin;
import eu.reapermaga.sco.system.file.yml.impl.ConfigFile;
import eu.reapermaga.sco.system.file.yml.impl.MaintenanceFile;
import lombok.Getter;

import java.io.File;

/**
 * Copyright (c) ReaperMaga, All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by ReaperMaga
 **/
@Getter
public class ConfigurationFiles {

    private ConfigFile configFile;
    private MaintenanceFile maintenanceFile;

    public ConfigurationFiles() {
        this.configFile = new ConfigFile();
        this.maintenanceFile = new MaintenanceFile();
    }

    public static final String getDefaultPath() {
        return "plugins" + File.separator + ScoPlugin.getInstance().getDescription().getName() + File.separator;
    }
}
