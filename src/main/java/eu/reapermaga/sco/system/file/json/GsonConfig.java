package eu.reapermaga.sco.system.file.json;

import lombok.Cleanup;
import lombok.Getter;
import lombok.SneakyThrows;

import java.io.*;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;

/**
 * Copyright (c) ReaperMaga, All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by ReaperMaga
 **/
@Getter
public abstract class GsonConfig<T> {

    private T entity;
    private File file;
    private File directory;

    public GsonConfig(String path, String fileName) {
        this.directory = new File(path);
        this.directory.mkdirs();
        this.file = new File(path.concat(fileName).concat(".json"));
        write();
    }

    @SneakyThrows
    public void write() {
        if(file.exists()){
            BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(file)));
            this.entity = GsonProvider.prettyGson().fromJson(reader, getGenericType(getClass(), 0));
        } else {
            this.entity = getDefault();
            save();
        }
    }

    public void setEntity(T entity) {
        this.entity = entity;
    }

    @SneakyThrows
    public void save() {
        String json = GsonProvider.prettyGson().toJson(entity);
        @Cleanup FileWriter writer = new FileWriter(file);
        writer.write(json);
        writer.flush();
    }

    public abstract T getDefault();

    protected Type getGenericType(Class<?> clazz, int typeIndex) {
        Type[] types = ((ParameterizedType) clazz.getGenericSuperclass()).getActualTypeArguments();
        return types[typeIndex];
    }

}
