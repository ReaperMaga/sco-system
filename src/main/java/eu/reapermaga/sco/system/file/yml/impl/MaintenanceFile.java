package eu.reapermaga.sco.system.file.yml.impl;

import com.google.common.collect.Lists;
import eu.reapermaga.sco.system.file.ConfigurationFiles;
import eu.reapermaga.sco.system.file.ext.DefaultCreation;
import eu.reapermaga.sco.system.file.yml.CoreConfig;
import lombok.Getter;

import java.util.List;
import java.util.Map;

/**
 * Copyright (c) ReaperMaga, All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by ReaperMaga
 **/
@Getter
public class MaintenanceFile extends CoreConfig implements DefaultCreation {

    private String message;

    public MaintenanceFile() {
        super(ConfigurationFiles.getDefaultPath(), "maintenance.yml");
        this.message = calcMessage();
    }

    @Override
    public void onCreate(Map<String, Object> configuration) {
        configuration.put("enable", false);

        List<String> messages = Lists.newArrayList();
        messages.add("&cSco Network");
        messages.add("&cThe server is on maintenance mode.");

        configuration.put("message", messages);

        List<String> players = Lists.newArrayList();
        players.add("ReaperMaga");

        configuration.put("players", players);
    }

    public void reload() {
        write();
        this.message = calcMessage();
    }

    public void clearPlayers() {
        List<String> players = getPlayers();
        players.clear();
        set("players", players);
        save();
    }

    public boolean isEnabled() {
        return getValue("enable");
    }

    public void setEnabled(boolean value){
        set("enable", value);
        save();
    }

    public String calcMessage(){
        List<String> oldMessages = getValue("message");
        int size = 1;
        String value = "";
        for(String old : oldMessages){
            value += old.replace("&", "§");
            if(size < oldMessages.size()){
                value += "\n";
            }
            size++;
        }
        return value;
    }

    public List<String> getPlayers() {
        return getValue("players");
    }

    public void removePlayer(String player){
        List<String> players = getPlayers();
        players.remove(player);
        set("players", players);
        save();
    }


    public void addPlayer(String player){
        List<String> players = getPlayers();
        players.add(player);
        set("players", players);
        save();
    }


    public String getPlayerByName(String player){
        for(String name : getPlayers()){
            if(name.equalsIgnoreCase(player)){
                return name;
            }
        }
        return "";
    }

    public boolean existsPlayer(String player){
        for(String name : getPlayers()){
            if(name.equalsIgnoreCase(player)){
                return true;
            }
        }
        return false;
    }
}
