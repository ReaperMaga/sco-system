package eu.reapermaga.sco.system.file.yml;


import eu.reapermaga.sco.system.file.ext.DefaultCreation;
import lombok.Cleanup;
import lombok.Getter;
import lombok.SneakyThrows;

import java.io.File;
import java.io.FileWriter;
import java.util.HashMap;
import java.util.Map;

/**
 * Copyright (c) ReaperMaga, All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by ReaperMaga
 **/
@Getter
public class CoreConfig {

    private File directory;
    private File file;

    private Map<String, Object> configuration;

    public CoreConfig(String path, String fileName) {
        this.directory = new File(path);
        if(!this.directory.exists()) {
            this.directory.mkdirs();
        }
        this.file = new File(path, fileName);
        write();
    }

    @SneakyThrows
    public void write() {
        if(!this.getFile().exists()) {
            this.getFile().createNewFile();
            this.configuration = new HashMap<>();
            if(this instanceof DefaultCreation) {
                DefaultCreation defaultCreation = (DefaultCreation)this;
                defaultCreation.onCreate(configuration);
                save();
            }
        } else {
            this.configuration = CoreConfigProvider.fromConfig(getFile(), Map.class);
        }
    }

    public boolean contains(String key) {
        return this.configuration.containsKey(key);
    }

    public void set(String key, Object value){
        configuration.put(key, value);
    }

    public <T> T getValue(String key) {
        if(!this.configuration.containsKey(key)) {
            return null;
        }
        return (T)this.configuration.get(key);
    }

    @SneakyThrows
    public void save() {
        final String output = CoreConfigProvider.toConfig(configuration);
        @Cleanup FileWriter fileWriter = new FileWriter(getFile());
        fileWriter.write(output);
        fileWriter.flush();
    }
}
