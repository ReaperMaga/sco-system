package eu.reapermaga.sco.system.file.yml;

import lombok.Cleanup;
import lombok.NonNull;
import lombok.SneakyThrows;
import org.yaml.snakeyaml.Yaml;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;

/**
 * Copyright (c) ReaperMaga, All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by ReaperMaga
 **/
public final class CoreConfigProvider {

    public static final Yaml YAML = new Yaml();

    @NonNull
    public static Yaml yaml() {
        return YAML;
    }

    public static <T> String toConfig(@NonNull T value) {
        final String output = yaml().dumpAsMap(value);
        return output;
    }

    @SneakyThrows
    public static <T> T fromConfig(@NonNull File file, Class<T> clazz) {
        @Cleanup BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(new FileInputStream(file)));
        final T object = yaml().loadAs(bufferedReader, clazz);
        return object;
    }

}
