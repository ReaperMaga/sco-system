package eu.reapermaga.sco.system.file.json;


import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import eu.reapermaga.sco.system.file.json.adapter.GsonLocationAdapter;
import org.bukkit.Location;

/**
 * Copyright (c) ReaperMaga, All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by ReaperMaga
 **/
public final class GsonProvider {

    private static final Gson PRETTY_GSON = new GsonBuilder()
            .serializeNulls()
            .disableHtmlEscaping()
            .setPrettyPrinting()
            .registerTypeAdapter(Location.class, new GsonLocationAdapter())
            .create();

    private static final Gson STANDARD_GSON = new GsonBuilder()
            .serializeNulls()
            .disableHtmlEscaping()
            .registerTypeAdapter(Location.class, new GsonLocationAdapter())
            .create();


    public static Gson prettyGson() {
        return PRETTY_GSON;
    }

    public static Gson standardGson() {
        return STANDARD_GSON;
    }
}
