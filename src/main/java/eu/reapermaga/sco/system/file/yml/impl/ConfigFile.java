package eu.reapermaga.sco.system.file.yml.impl;

import eu.reapermaga.sco.system.file.ConfigurationFiles;
import eu.reapermaga.sco.system.file.ext.DefaultCreation;
import eu.reapermaga.sco.system.file.yml.CoreConfig;

import java.util.Map;

/**
 * Copyright (c) ReaperMaga, All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by ReaperMaga
 **/
public class ConfigFile extends CoreConfig implements DefaultCreation {

    public ConfigFile() {
        super(ConfigurationFiles.getDefaultPath(), "config.yml");
    }


    @Override
    public void onCreate(Map<String, Object> configuration) {
        configuration.put("grass_radius", 5);
        configuration.put("clearlag_seconds", 180);
        configuration.put("globalmute", false);


        configuration.put("join_message", "§8[§a+§8] §7%player%");
        configuration.put("quit_message", "§8[§c-§8] §7%player%");
    }

    public boolean isGlobalmute() {
        return getValue("globalmute");
    }

    public void setGlobalmute(boolean value){
        set("globalmute", value);
        save();
    }

    public String getJoinMessage(String player) {
        String message = getValue("join_message");
        return message.replace("%player%", player);
    }

    public String getQuitMessage(String player) {
        String message = getValue("quit_message");
        return message.replace("%player%", player);
    }

    public void changeGrassRadius(int radius) {
        set("grass_radius", radius);
        save();
    }

    public int getClearlagSeconds() {
        return getValue("clearlag_seconds");
    }

    public int getGrassRadius() {
        return getValue("grass_radius");
    }
}
