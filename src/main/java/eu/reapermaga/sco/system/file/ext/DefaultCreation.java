package eu.reapermaga.sco.system.file.ext;

import java.util.Map;

/**
 * Copyright (c) ReaperMaga, All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by ReaperMaga
 **/
public interface DefaultCreation {

    void onCreate(Map<String, Object> configuration);
}
