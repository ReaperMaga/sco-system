package eu.reapermaga.sco.system.task;

import eu.reapermaga.sco.system.user.SpigotUser;
import eu.reapermaga.sco.system.util.SpigotUtil;
import org.bukkit.Material;
import org.bukkit.block.Block;

/**
 * Copyright (c) ReaperMaga, All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by ReaperMaga
 **/
public class GrassTask implements Runnable{

    @Override
    public void run() {
        for(SpigotUser user : SpigotUser.USERS) {
            if(user.getPlayer().isOnline()) {
                if(!user.getQueueGrass().isEmpty()){
                    Block block = user.pollGrass();
                    SpigotUtil.sendBlockChange(user.getPlayer(), block.getLocation(), block.getType(), block.getData());

                }

            }
        }
    }
}
