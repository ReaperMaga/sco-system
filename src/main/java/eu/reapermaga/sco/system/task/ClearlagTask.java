package eu.reapermaga.sco.system.task;

import eu.reapermaga.sco.system.io.Message;
import eu.reapermaga.sco.system.user.SpigotUser;
import eu.reapermaga.sco.system.user.obj.Settings;
import lombok.Getter;
import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Item;

/**
 * Copyright (c) ReaperMaga, All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by ReaperMaga
 **/
@Getter
public class ClearlagTask implements Runnable{

    private final int maxCount;
    private int current;

    public ClearlagTask(int maxCount) {
        this.maxCount = maxCount;
        this.current = maxCount;
    }


    @Override
    public void run() {
        if(current == 60 || current == 30 || current == 10 || current == 5 || current == 4 || current == 3 || current == 2 || current == 1) {
            String show = current == 1 ? "second" : "seconds";
            sendAll("§7Ground items will be removed in §c" + current + " " + show + "§7.");
            current--;
        } else if(current == 0){
            int count = 0;
            for(World world : Bukkit.getWorlds()){
                for(Entity entity : world.getEntities()){
                    if(entity instanceof Item){
                        entity.remove();
                        count++;
                    }
                }
            }
            sendAll("§7Removed §8(§c"+count+"§8) §7Ground items.");
            current = getMaxCount();
        } else {
            current--;
        }

    }

    private void sendAll(String message){
        for(SpigotUser all : SpigotUser.USERS){
            if(all.isSetting(Settings.CLEARLAG_NOTIFICATIONS)){
                all.sendMessage(message);
            }
        }
    }
}
