package eu.reapermaga.sco.system;

import eu.reapermaga.sco.system.command.impl.*;
import eu.reapermaga.sco.system.command.impl.maintenance.MaintenanceCommand;
import eu.reapermaga.sco.system.command.impl.outpost.OutpostCommand;
import eu.reapermaga.sco.system.command.impl.sco.ScoCommand;
import eu.reapermaga.sco.system.database.ext.Credentials;
import eu.reapermaga.sco.system.database.mongo.MongoContext;
import eu.reapermaga.sco.system.file.ConfigurationFiles;
import eu.reapermaga.sco.system.listener.entity.EntityDamageListener;
import eu.reapermaga.sco.system.listener.player.*;
import eu.reapermaga.sco.system.listener.world.LeavesDecayListener;
import eu.reapermaga.sco.system.location.LocationHandler;
import eu.reapermaga.sco.system.outpost.OutpostHandler;
import eu.reapermaga.sco.system.task.ClearlagTask;
import eu.reapermaga.sco.system.task.GrassTask;
import eu.reapermaga.sco.system.user.SpigotUser;
import eu.reapermaga.sco.system.util.setup.Setup;
import lombok.Getter;
import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * Copyright (c) ReaperMaga, All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by ReaperMaga
 **/
@Getter
public class ScoPlugin extends JavaPlugin {

    @Getter
    private static ScoPlugin instance;

    private ConfigurationFiles configurationFiles;
    private LocationHandler locationHandler;
    private OutpostHandler outpostHandler;

    @Override
    public void onLoad() {
        instance = this;
    }

    @Override
    public void onEnable() {
        initialize();
    }

    @Override
    public void onDisable() {
        SpigotUser.saveOnlineUsers();
    }

    private void initialize() {
        this.configurationFiles = new ConfigurationFiles();
        this.locationHandler = new LocationHandler();
        this.outpostHandler = new OutpostHandler();

        //------------------------------------------
        registerSetup();
        registerTasks();

        SpigotUser.registerOnlineUsers();
    }

    private void registerSetup() {
        Setup setup = new Setup(this);
        /**
         * Commands
         */
        setup.addCommand(new ScoCommand());
        setup.addCommand(new TeamchatCommand());
        setup.addCommand(new CmdSpyCommand());
        setup.addCommand(new MessageCommand());
        setup.addCommand(new ReplyCommand());
        setup.addCommand(new SettingsCommand());
        setup.addCommand(new GameModeCommand());
        setup.addCommand(new PingCommand());
        setup.addCommand(new HealCommand());
        setup.addCommand(new FoodCommand());
        setup.addCommand(new FlyCommand());
        setup.addCommand(new MaintenanceCommand());
        setup.addCommand(new ClearCommand());
        setup.addCommand(new TestCommand());
        setup.addCommand(new ClearChatCommand());
        setup.addCommand(new GlobalMuteCommand());
        setup.addCommand(new BroadcastCommand());
        setup.addCommand(new SpawnCommand());
        setup.addCommand(new SetSpawnCommand());
        setup.addCommand(new DayCommand());
        setup.addCommand(new NightCommand());
        setup.addCommand(new GodCommand());
        setup.addCommand(new SkullCommand());
        setup.addCommand(new OutpostCommand());

        /**
         * Listener
         */
        setup.addListener(new PlayerMoveListener());
        setup.addListener(new PlayerCommandListener());
        setup.addListener(new LeavesDecayListener());
        setup.addListener(new PlayerJoinListener());
        setup.addListener(new PlayerQuitListener());
        setup.addListener(new PlayerLoginListener());
        setup.addListener(new PlayerChatListener());
        setup.addListener(new EntityDamageListener());
        setup.addListener(new PlayerInteractListener());


        //--------------------------------
        setup.register();
    }

    private void registerTasks() {
        Bukkit.getScheduler().runTaskTimer(this, new GrassTask(), 20, 2);
        Bukkit.getScheduler().runTaskTimer(this, new ClearlagTask(ScoPlugin.getInstance().getConfigurationFiles().getConfigFile().getClearlagSeconds()), 20, 20);
    }


}
