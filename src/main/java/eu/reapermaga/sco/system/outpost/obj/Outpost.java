package eu.reapermaga.sco.system.outpost.obj;

import eu.reapermaga.sco.system.util.math.Cuboid;
import lombok.Getter;
import lombok.Setter;
import org.bukkit.Location;

/**
 * Copyright (c) ReaperMaga, All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by ReaperMaga
 **/
@Getter
@Setter
public class Outpost {

    private String name;
    private Location center;
    private Cuboid region;


    @Override
    public String toString() {
        return this.name;
    }
}
