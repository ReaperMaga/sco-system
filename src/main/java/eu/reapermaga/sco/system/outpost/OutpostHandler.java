package eu.reapermaga.sco.system.outpost;

import com.google.common.collect.Lists;
import eu.reapermaga.sco.system.file.ConfigurationFiles;
import eu.reapermaga.sco.system.file.json.GsonConfig;
import eu.reapermaga.sco.system.outpost.obj.Outpost;
import eu.reapermaga.sco.system.util.inventory.util.ItemCreator;
import eu.reapermaga.sco.system.util.math.Cuboid;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

import java.util.List;

/**
 * Copyright (c) ReaperMaga, All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by ReaperMaga
 **/
public class OutpostHandler extends GsonConfig<List<Outpost>> {

    public static final ItemStack REGION_WAND = new ItemCreator().setMaterial(Material.STICK).setName("§dOutpost-Wand").glow().build();

    public static final String LOCATIOM_A_KEY = "outpost_location_a";
    public static final String LOCATIOM_B_KEY = "outpost_location_b";

    public OutpostHandler() {
        super(ConfigurationFiles.getDefaultPath(), "outposts");
    }

    public void createOutpost(String name, Location center, Location locationA, Location locationB) {
        Outpost outpost = new Outpost();
        outpost.setName(name);
        outpost.setCenter(center);
        outpost.setRegion(new Cuboid(locationA, locationB));

        getEntity().add(outpost);
    }

    public void deleteOutpost(Outpost outpost) {
        getEntity().remove(outpost);
        save();
    }

    public boolean existsOutpostByName(String name){
        for(Outpost outpost : getEntity()) {
            if(outpost.getName().equalsIgnoreCase(name)) {
                return true;
            }
        }
        return false;
    }

    public Outpost getOutpostByName(String name){
        for(Outpost outpost : getEntity()) {
            if(outpost.getName().equalsIgnoreCase(name)) {
                return outpost;
            }
        }
        return null;
    }

    @Override
    public List<Outpost> getDefault() {
        return Lists.newArrayList();
    }
}
