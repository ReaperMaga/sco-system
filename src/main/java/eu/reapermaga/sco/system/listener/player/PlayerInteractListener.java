package eu.reapermaga.sco.system.listener.player;

import eu.reapermaga.sco.system.io.Message;
import eu.reapermaga.sco.system.outpost.OutpostHandler;
import eu.reapermaga.sco.system.user.SpigotUser;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerInteractEvent;

/**
 * Copyright (c) ReaperMaga, All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by ReaperMaga
 **/
public class PlayerInteractListener implements Listener {

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onCall(PlayerInteractEvent event) {
        if(event.getItem() != null) {
            if(event.getItem().hasItemMeta()) {
                if(event.getItem().getItemMeta().hasDisplayName()) {
                    if(!event.getItem().getItemMeta().getDisplayName().equals(OutpostHandler.REGION_WAND.getItemMeta().getDisplayName())) {
                        return;
                    }
                    Player player = event.getPlayer();
                    if(!player.hasPermission(Message.PERMISSION_PREFIX + "outpost")) {
                        return;
                    }
                    SpigotUser user = SpigotUser.getSpigotUser(player);
                    if(event.getAction() == Action.RIGHT_CLICK_BLOCK) {
                        user.setTag(OutpostHandler.LOCATIOM_A_KEY, event.getClickedBlock().getLocation());
                        user.sendMessage("§aLocation A set");
                    } else if(event.getAction() == Action.LEFT_CLICK_BLOCK) {
                        user.setTag(OutpostHandler.LOCATIOM_B_KEY, event.getClickedBlock().getLocation());
                        user.sendMessage("§aLocation B set");
                    }
                }
            }
        }



    }
}
