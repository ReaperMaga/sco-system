package eu.reapermaga.sco.system.listener.world;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.LeavesDecayEvent;

/**
 * Copyright (c) ReaperMaga, All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by ReaperMaga
 **/
public class LeavesDecayListener implements Listener {

    @EventHandler
    public void onCall(LeavesDecayEvent event){
        event.setCancelled(true);
    }
}
