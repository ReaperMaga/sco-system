package eu.reapermaga.sco.system.listener.player;

import eu.reapermaga.sco.system.ScoPlugin;
import eu.reapermaga.sco.system.user.SpigotUser;
import eu.reapermaga.sco.system.user.obj.Settings;
import eu.reapermaga.sco.system.util.SpigotUtil;
import net.minecraft.server.v1_12_R1.BlockPosition;
import net.minecraft.server.v1_12_R1.PacketPlayOutBlockChange;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.craftbukkit.v1_12_R1.CraftWorld;
import org.bukkit.craftbukkit.v1_12_R1.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;

/**
 * Copyright (c) ReaperMaga, All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by ReaperMaga
 **/
public class PlayerMoveListener implements Listener {

    @EventHandler
    public void onCall(PlayerMoveEvent event) {
        Player player = event.getPlayer();
        SpigotUser user = SpigotUser.getSpigotUser(player);
        if(!user.isSetting(Settings.GRASS_REMOVE)){
            return;
        }
        for(Location location : SpigotUtil.getCircleBlocks(player.getLocation(), ScoPlugin.getInstance().getConfigurationFiles().getConfigFile().getGrassRadius())) {
            if(location.getBlock().getType() == Material.LONG_GRASS || location.getBlock().getType() == Material.DOUBLE_PLANT
                    || location.getBlock().getType() == Material.YELLOW_FLOWER || location.getBlock().getType() == Material.CHORUS_FLOWER
                    || location.getBlock().getType() == Material.RED_ROSE) {
                SpigotUtil.sendBlockChange(player, location.getBlock().getLocation(), Material.AIR);
                if(!user.containsGrassBlock(location.getBlock())) {
                    user.addGrass(location.getBlock());
                }

            }

        }
    }
}
