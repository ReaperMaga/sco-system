package eu.reapermaga.sco.system.listener.entity;

import eu.reapermaga.sco.system.command.impl.GodCommand;
import eu.reapermaga.sco.system.user.SpigotUser;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;

/**
 * Copyright (c) ReaperMaga, All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by ReaperMaga
 **/
public class EntityDamageListener implements Listener {

    @EventHandler
    public void onCall(EntityDamageEvent event) {
        if(event.getEntityType() == EntityType.PLAYER) {
            Player player = (Player)event.getEntity();
            SpigotUser user = SpigotUser.getSpigotUser(player);
            if(user.containsTag(GodCommand.GOD_KEY)) {
                event.setCancelled(true);
                return;
            }
        }
     }


}
