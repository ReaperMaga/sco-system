package eu.reapermaga.sco.system.listener.player;

import eu.reapermaga.sco.system.ScoPlugin;
import eu.reapermaga.sco.system.command.impl.CmdSpyCommand;
import eu.reapermaga.sco.system.file.yml.impl.ConfigFile;
import eu.reapermaga.sco.system.io.Message;
import eu.reapermaga.sco.system.user.SpigotUser;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.help.HelpTopic;

/**
 * Erstellt von ItsMaga
 */
public class PlayerCommandListener implements Listener {

    private static final String[] BLOCKED_COMMANDS = {"/me", "/minecraft:me", "/pl", "/plugins", "/bukkit:plugins", "/bukkit:pl", "/bukkit:?", "?"};

    @EventHandler
    public void onCall(PlayerCommandPreprocessEvent event){
        Player player = event.getPlayer();
        SpigotUser user = SpigotUser.getSpigotUser(player);
        String command = event.getMessage().split(" ")[0];
        HelpTopic topic = Bukkit.getServer().getHelpMap().getHelpTopic(command);
        if(topic == null){
            /*event.setCancelled(true);
            user.sendMessage("§7Command §8[§c" + event.getMessage() + "§8] §7not found.");*/
        } else {
            if(handleBlockedCommands(user, event)){
                return;
            }
            handlePrivateMessage(user, event);
            handleCommandSpy(user, event);
        }
    }

    private boolean handleBlockedCommands(SpigotUser user, PlayerCommandPreprocessEvent event){
        if(user.getPlayer().hasPermission(Message.PERMISSION_PREFIX + "command.bypass"))  {
            return false;
        }
        final String message = event.getMessage().toLowerCase();
        for(String blockedCommand : BLOCKED_COMMANDS){
            if(message.startsWith(blockedCommand)){
                event.setCancelled(true);
                user.sendMessage("§cYou are not allowed to use this command");
                return true;
            }
        }
        return false;
    }


    private void handleCommandSpy(SpigotUser user, PlayerCommandPreprocessEvent event){
        for(SpigotUser all : SpigotUser.USERS){
            if(all.containsTag(CmdSpyCommand.SPY_KEY)){
                if(all.getPlayer().getName().equals(user.getPlayer().getName())){
                    continue;
                }
                all.sendMessage(Message.COMMAND_SPY_PREFIX + user.getPlayer().getName() + " §8➥ §7" + event.getMessage());
            }
        }
    }

    private void handlePrivateMessage(SpigotUser user, PlayerCommandPreprocessEvent event){
        final String message = event.getMessage().toLowerCase();
        if(message.startsWith("/msg") || message.startsWith("/r") || message.startsWith("/reply")
                || message.startsWith("message") || message.startsWith("tell") || message.startsWith("minecraft:tell")) {
            ConfigFile file = ScoPlugin.getInstance().getConfigurationFiles().getConfigFile();
            if(file.isGlobalmute()){
                if(!user.getPlayer().hasPermission(Message.PERMISSION_PREFIX + "globalmute.bypass"))  {
                    event.setCancelled(true);
                    user.sendMessage("The GlobalMute is §con§7.");
                    return;
                }
            }


        }

    }
}
