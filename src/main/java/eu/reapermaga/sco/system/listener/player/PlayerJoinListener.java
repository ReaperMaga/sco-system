package eu.reapermaga.sco.system.listener.player;

import eu.reapermaga.sco.system.ScoPlugin;
import eu.reapermaga.sco.system.io.Message;
import eu.reapermaga.sco.system.user.SpigotUser;
import eu.reapermaga.sco.system.util.SpigotUtil;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

/**
 * Copyright (c) ReaperMaga, All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by ReaperMaga
 **/
public class PlayerJoinListener implements Listener {

    @EventHandler
    public void onCall(PlayerJoinEvent event) {
        Player player = event.getPlayer();
        SpigotUser user = SpigotUser.getSpigotUser(player);

        String joinMessage = ScoPlugin.getInstance().getConfigurationFiles().getConfigFile().getJoinMessage(player.getName());
        if(joinMessage.isEmpty()){
            event.setJoinMessage(null);
        } else {
            event.setJoinMessage(joinMessage);
        }


        /*for(Player all : Bukkit.getOnlinePlayers()) {
            sendTabTitle(all, (Bukkit.getOnlinePlayers().size()));
        }*/


    }

    public static void sendTabTitle(Player player, int players){
        final StringBuilder header = new StringBuilder("§c\n").append("§8* §b§lSwordCraftOnline §8*").append("\n")
                .append("§8* §7There are §3" + players + "§8/§3" + Bukkit.getMaxPlayers() + " §7players online §8*").append("\n§c ");

        final StringBuilder footer = new StringBuilder("\n")
                .append("§8* §bWebsite§8: §7https://www.ancientrpg.net/ §8*").append("\n")
                .append("  §8* §bDiscord§8: §7https://discord.gg/GA6YbUQ §8*  ").append("\n\n")
                .append("§8* §7§oServer slogan §8*").append("\n§c");


        SpigotUtil.setTabtitle(player, header.toString(), footer.toString());
    }
}
