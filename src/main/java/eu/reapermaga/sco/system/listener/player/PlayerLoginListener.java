package eu.reapermaga.sco.system.listener.player;

import eu.reapermaga.sco.system.ScoPlugin;
import eu.reapermaga.sco.system.file.yml.impl.MaintenanceFile;
import eu.reapermaga.sco.system.user.SpigotUser;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerLoginEvent;

/**
 * Copyright (c) ReaperMaga, All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by ReaperMaga
 **/
public class PlayerLoginListener implements Listener {

    @EventHandler
    public void onCall(PlayerLoginEvent event){
        Player player = event.getPlayer();
        if(handleMaintenance(player, event)){
            return;
        }
        SpigotUser user = SpigotUser.getSpigotUser(player);
    }

    private boolean handleMaintenance(Player player, PlayerLoginEvent event){
        MaintenanceFile file = ScoPlugin.getInstance().getConfigurationFiles().getMaintenanceFile();
        if(file.isEnabled()){
            if(!file.existsPlayer(player.getName())){
                event.setResult(PlayerLoginEvent.Result.KICK_OTHER);
                event.setKickMessage(file.getMessage());
                return true;
            }
        }
        return false;
    }


}
