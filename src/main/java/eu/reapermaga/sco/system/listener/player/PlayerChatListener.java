package eu.reapermaga.sco.system.listener.player;

import eu.reapermaga.sco.system.ScoPlugin;
import eu.reapermaga.sco.system.file.yml.impl.ConfigFile;
import eu.reapermaga.sco.system.io.Message;
import eu.reapermaga.sco.system.user.SpigotUser;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

/**
 * Copyright (c) ReaperMaga, All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by ReaperMaga
 **/
public class PlayerChatListener implements Listener {

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onCall(AsyncPlayerChatEvent event) {
        Player player = event.getPlayer();
        SpigotUser user = SpigotUser.getSpigotUser(player);
        handleGlobalmute(user, event);
    }

    private boolean handleGlobalmute(SpigotUser user, AsyncPlayerChatEvent event){
        ConfigFile file = ScoPlugin.getInstance().getConfigurationFiles().getConfigFile();
        if(file.isGlobalmute()){
            if(!user.getPlayer().hasPermission(Message.PERMISSION_PREFIX + "globalmute.bypass"))  {
                event.setCancelled(true);
                user.sendMessage("§cThe GlobalMute is currently on.");
                return true;
            }
        }
        return false;
    }
}
