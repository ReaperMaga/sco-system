package eu.reapermaga.sco.system.listener.player;

import eu.reapermaga.sco.system.ScoPlugin;
import eu.reapermaga.sco.system.user.SpigotUser;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

/**
 * Copyright (c) ReaperMaga, All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by ReaperMaga
 **/
public class PlayerQuitListener implements Listener {

    @EventHandler
    public void onCall(PlayerQuitEvent event) {
        Player player = event.getPlayer();
        SpigotUser user = SpigotUser.getSpigotUser(player);
        user.saveData();

        String quitMessage = ScoPlugin.getInstance().getConfigurationFiles().getConfigFile().getQuitMessage(player.getName());
        if(quitMessage.isEmpty()){
            event.setQuitMessage(null);
        } else {
            event.setQuitMessage(quitMessage);
        }
    }
}
