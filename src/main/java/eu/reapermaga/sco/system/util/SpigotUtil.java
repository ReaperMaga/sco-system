package eu.reapermaga.sco.system.util;

import eu.reapermaga.sco.system.util.text.DefaultFontInfo;
import net.minecraft.server.v1_12_R1.*;
import org.bukkit.*;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.craftbukkit.v1_12_R1.CraftWorld;
import org.bukkit.craftbukkit.v1_12_R1.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Copyright (c) ReaperMaga, All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by ReaperMaga
 **/
public class SpigotUtil {


    private final static int CENTER_PX = 154;
    private static final Random RANDOM = new Random();
    private static final String TEMPLATE = "§8[§7=§7=§7=§7=§7=§7=§7=§7=§7=§7=§7=§7=§7=§7=§7=§7=§7=§7=§7=§7=§8] §a0%";
    private static final int SPACERS = 20;
    private static final String CHARACTER = "§a=";

    private static final double SMOOTHNESS = 0.5;

    public static void setTabtitle(Player player, String header, String footer) {
        if (header == null) {
            header = "";
        }
        if (footer == null) {
            footer = "";
        }
        PlayerConnection connection = ((CraftPlayer) player).getHandle().playerConnection;

        IChatBaseComponent tabTitle = IChatBaseComponent.ChatSerializer.a("{\"text\": \"" + header + "\"}");

        IChatBaseComponent tabFoot = IChatBaseComponent.ChatSerializer.a("{\"text\": \"" + footer + "\"}");

        PacketPlayOutPlayerListHeaderFooter headerPacket = new PacketPlayOutPlayerListHeaderFooter();
        try {
            Field field = headerPacket.getClass().getDeclaredField("b");
            field.setAccessible(true);
            field.set(headerPacket, tabFoot);

            Field fieldA = headerPacket.getClass().getDeclaredField("a");
            fieldA.setAccessible(true);
            fieldA.set(headerPacket, tabTitle);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            connection.sendPacket(headerPacket);

        }
    }

    public static void addItem(Player player, ItemStack item){
        if(player.getInventory().firstEmpty() == -1){
            player.getLocation().getWorld().dropItem(player.getLocation(), item);
        } else {
            player.getInventory().addItem(item);
        }
    }

    public static String printProgress(long max, long current){
        String pattern = TEMPLATE;
        final long spacerReplaces = (current * SPACERS)/max;

        for (int i = 0; i < spacerReplaces; i++) {
            pattern = pattern.replaceFirst("§7=", CHARACTER);
        }

        pattern = pattern.replaceFirst("0", Long.toString((current * 100)/max));

        return pattern;
    }


    public static List<Location> getCircleBlocksFlat(Location center, int radius) {
        List<Location> locations = new ArrayList<>();

        int centerX = center.getBlockX();
        int centerZ = center.getBlockZ();
        double radiusSqrt = (radius+SMOOTHNESS) * (radius+SMOOTHNESS);

        for (int z = - radius; z <=  radius; z++) {
            for (int x = - radius; x <=  radius; x++) {

                double distSqrt = z * z + x * x;
                if (distSqrt <= radiusSqrt) {
                    locations.add(new Location(center.getWorld(), centerX + x, center.getY(), centerZ + z));
                }
            }
        }
        return locations;
    }

    public static List<Location> getCircleBlocks(Location center, int radius) {
        List<Location> locations = new ArrayList<>();

        int centerX = center.getBlockX();
        int centerZ = center.getBlockZ();
        double radiusSqrt = (radius+SMOOTHNESS) * (radius+SMOOTHNESS);

        for (int z = - radius; z <=  radius; z++) {
            for (int x = - radius; x <=  radius; x++) {

                double distSqrt = z * z + x * x;
                if (distSqrt <= radiusSqrt) {
                    locations.add(new Location(center.getWorld(), centerX + x, center.getY(), centerZ + z));
                    locations.add(new Location(center.getWorld(), centerX + x, center.getY() + 1.0, centerZ + z));
                    locations.add(new Location(center.getWorld(), centerX + x, center.getY() + 2.0, centerZ + z));
                    locations.add(new Location(center.getWorld(), centerX + x, center.getY() - 1.0, centerZ + z));
                }
            }
        }
        return locations;
    }


    public static void sendBlockChange(Player player, Location location, Material material) {
        BlockPosition blockPosition = new BlockPosition(location.getBlockX(), location.getBlockY(), location.getBlockZ());
        PacketPlayOutBlockChange blockChange = new PacketPlayOutBlockChange(((CraftWorld) location.getWorld()).getHandle(), blockPosition);
        blockChange.block = net.minecraft.server.v1_12_R1.Block.getByCombinedId(material.getId());

        ((CraftPlayer)player).getHandle().playerConnection.sendPacket(blockChange);
    }


    public static void sendBlockChange(Player player, Location location, Material material, byte data) {
        BlockPosition blockPosition = new BlockPosition(location.getBlockX(), location.getBlockY(), location.getBlockZ());
        PacketPlayOutBlockChange blockChange = new PacketPlayOutBlockChange(((CraftWorld) location.getWorld()).getHandle(), blockPosition);
        blockChange.block = net.minecraft.server.v1_12_R1.Block.getByCombinedId(material.getId()  + (data << 12));

        ((CraftPlayer)player).getHandle().playerConnection.sendPacket(blockChange);
    }

    public static boolean isNumber(String text){
        try {
            Integer.valueOf(text);
            return true;
        }catch (NumberFormatException ex){
            return false;
        }
    }

    public static String formatMoney(long money){
        return String.format("%,d", (money));
    }

    /** Serialize String to location **/
    public static Location stringToLocation(String stringLocation){
        String[] args = stringLocation.split(";");
        World world = Bukkit.getWorld(args[0]);
        double x = Double.valueOf(args[1]);
        double y = Double.valueOf(args[2]);
        double z = Double.valueOf(args[3]);
        double yaw = Double.valueOf(args[4]);
        double pitch = Double.valueOf(args[5]);
        return new Location(world, x, y, z, (float)yaw, (float)pitch);
    }

    /** Deserialize Location to string **/
    public static String locationToString(Location location){
        StringBuilder builder = new StringBuilder()
                .append(location.getWorld().getName()).append(";")
                .append(location.getX()).append(";")
                .append(location.getY()).append(";")
                .append(location.getZ()).append(";")
                .append(location.getYaw()).append(";")
                .append(location.getPitch());
        return builder.toString();
    }

    public static void sendCenteredMessage(Player player, String message){
        if(message == null || message.equals("")) player.sendMessage("");
        message = ChatColor.translateAlternateColorCodes('&', message);

        int messagePxSize = 0;
        boolean previousCode = false;
        boolean isBold = false;

        for(char c : message.toCharArray()){
            if(c == '§'){
                previousCode = true;
                continue;
            }else if(previousCode == true){
                previousCode = false;
                if(c == 'l' || c == 'L'){
                    isBold = true;
                    continue;
                }else isBold = false;
            }else{
                DefaultFontInfo dFI = DefaultFontInfo.getDefaultFontInfo(c);
                messagePxSize += isBold ? dFI.getBoldLength() : dFI.getLength();
                messagePxSize++;
            }
        }

        int halvedMessageSize = messagePxSize / 2;
        int toCompensate = CENTER_PX - halvedMessageSize;
        int spaceLength = DefaultFontInfo.SPACE.getLength() + 1;
        int compensated = 0;
        StringBuilder sb = new StringBuilder();
        while(compensated < toCompensate){
            sb.append(" ");
            compensated += spaceLength;
        }
        player.sendMessage(sb.toString() + message);
    }

}
