package eu.reapermaga.sco.system.util.inventory.ext;

import org.bukkit.entity.Player;

/**
 * Copyright (c) ReaperMaga, All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by ReaperMaga, 27.09.18
 **/
public interface Closeable {

    void onClose(Player player);
}
