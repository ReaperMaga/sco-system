package eu.reapermaga.sco.system.util.inventory.type;


import eu.reapermaga.sco.system.util.inventory.InventoryBuilder;

/**
 * Copyright (c) ReaperMaga, All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by ReaperMaga, 21.09.18
 **/
public interface InventoryBuilderType {

    void setContents(InventoryBuilder builder);
}
