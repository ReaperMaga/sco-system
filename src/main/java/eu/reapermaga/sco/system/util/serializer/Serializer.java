package eu.reapermaga.sco.system.util.serializer;

/**
 * Copyright (c) ReaperMaga, All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by ReaperMaga, 19.09.18
 **/
public interface Serializer<T, K> {

    T serialize(K k);
    K deserialize(T k);
}
