package eu.reapermaga.sco.system.util.inventory.util;

import eu.reapermaga.sco.system.io.Message;
import eu.reapermaga.sco.system.util.inventory.skull.CustomSkull;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

/**
 * Copyright (c) ReaperMaga, All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by ReaperMaga
 **/
public final class InventoryConstants {

    public static final ItemStack BUTTON = new ItemCreator().setMaterial(Material.STONE_BUTTON).setName("§c").build();
    public static final ItemStack ORANGE_CLASS = new ItemCreator().setMaterial(Material.STAINED_GLASS_PANE).setData(1).setName("§c").build();
    public static final ItemStack BACK_ITEM = CustomSkull.getHead(CustomSkull.SkullType.ARROW_DOWN, Message.ARROW + "§cBack");
    public static final ItemStack BACK_DOOR_ITEM = new ItemCreator().setMaterial(Material.WOOD_DOOR).setName(Message.ARROW + "§cBack").build();
}
