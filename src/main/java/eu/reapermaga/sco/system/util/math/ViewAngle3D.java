package eu.reapermaga.sco.system.util.math;


import eu.reapermaga.sco.system.util.math.location.Location2D;
import eu.reapermaga.sco.system.util.math.location.Location3D;

public class ViewAngle3D {
	double yaw;
	double pitch;

	
	public ViewAngle3D(Location3D start, Location3D target){
		double yawX = new Location2D(start.getX(), start.getZ()).distance(new Location2D(target.getX(), target.getZ()));
		double yawY = target.getY() - start.getY();

		double YawrunterRechnen = new Location2D(0, 0).distance(new Location2D(yawX, yawY));

		yawX /= YawrunterRechnen;
		yawY /= YawrunterRechnen;

		double yaw = - Math.toDegrees(Math.asin(yawY));
		double pitchX = target.getX() - start.getX();
		double pitchY = target.getZ() - start.getZ();

		double PitchrunterRechnen = new Location2D(0, 0).distance(new Location2D(pitchX, pitchY));
		
		pitchX /= PitchrunterRechnen;
		pitchY /= PitchrunterRechnen;
		
		double pitch =Math.toDegrees(Math.asin(pitchY));
		
		pitch -=90;
		
		if(start.getX() > target.getX()){
			pitch *= -1;
		}
		
		this.yaw = yaw;
		this.pitch = pitch;
	}
	
	public double getPitch(){
		return this.yaw;
	}
	
	public double getYaw(){
		return this.pitch;
	}

}