package eu.reapermaga.sco.system.util.setup;

import com.google.common.collect.Lists;
import lombok.Getter;
import lombok.NonNull;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.craftbukkit.v1_12_R1.CraftServer;
import org.bukkit.event.Listener;
import org.bukkit.plugin.Plugin;

import java.util.List;

/**
 * Copyright (c) ReaperMaga, All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by ReaperMaga
 **/
@Getter
public class Setup {

    private final Plugin plugin;
    private final List<Command> commands;
    private final List<Listener> listeners;

    /**
     * Constructor
     * @param plugin = the main class that extends JavaPlugin
     */
    public Setup(@NonNull Plugin plugin) {
        this.plugin = plugin;
        this.commands = Lists.newArrayList();
        this.listeners = Lists.newArrayList();
    }

    /**
     * Add a command to list
     *
     * WARNING: It is not the default CommandExecutor
     * @param command = bukkit command
     */
    public void addCommand(@NonNull Command command){
        this.commands.add(command);
    }

    /**
     * Add a listener to list
     * @param listener = bukkit listener
     */
    public void addListener(@NonNull Listener listener){
        this.listeners.add(listener);
    }

    /**
     * Register command & listener
     * Without putting the command into the plugin.yml
     */
    public void register(){
        CraftServer server = (CraftServer) Bukkit.getServer();
        this.commands.forEach((command -> {
            server.getCommandMap().register("plugin", command);
        }));
        this.listeners.forEach((listener -> {
            getPlugin().getServer().getPluginManager().registerEvents(listener, getPlugin());
        }));
    }
}
