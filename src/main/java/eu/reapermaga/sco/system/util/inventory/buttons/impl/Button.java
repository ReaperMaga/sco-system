package eu.reapermaga.sco.system.util.inventory.buttons.impl;


/**
 * Copyright (c) ReaperMaga, All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by ReaperMaga, 22.08.2018
 **/
public interface Button {

    /**
     * Empty Button
     */
}
