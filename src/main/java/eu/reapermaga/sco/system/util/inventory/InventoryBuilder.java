package eu.reapermaga.sco.system.util.inventory;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import eu.reapermaga.sco.system.util.inventory.animation.AnimationInventory;
import eu.reapermaga.sco.system.util.inventory.buttons.InventoryButton;
import eu.reapermaga.sco.system.util.inventory.buttons.impl.Button;
import eu.reapermaga.sco.system.util.inventory.buttons.impl.ClickButton;
import eu.reapermaga.sco.system.util.inventory.ext.Closeable;
import eu.reapermaga.sco.system.util.inventory.ext.Updateable;
import eu.reapermaga.sco.system.util.inventory.skull.CustomSkull;
import eu.reapermaga.sco.system.util.inventory.type.InventoryBuilderType;
import eu.reapermaga.sco.system.util.inventory.util.ItemCreator;
import eu.reapermaga.sco.system.util.tuple.ImmutableTuple;
import lombok.Getter;
import lombok.SneakyThrows;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.*;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.Plugin;
import org.bukkit.scheduler.BukkitRunnable;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * Copyright (c) ReaperMaga, All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by ReaperMaga, 22.08.2018
 **/
@Getter
public class InventoryBuilder implements Serializable, Cloneable {

    /**
     * Initalize register listener
     */
    private static boolean INITALIZED = false;

    public static void initalize(Plugin plugin) {
        if (!INITALIZED) {
            INITALIZED = true;
            plugin.getServer().getPluginManager().registerEvents(new InventoryBuilderListener(), plugin);
            new InventoryBuilderTask().runTaskTimer(plugin, 20L, 20L);
        }
    }


    /**
     * Static constants
     */
    public static final List<InventoryBuilder> BUILDERS = Lists.newCopyOnWriteArrayList();
    public static final List<String> FIXED_TITLES = Lists.newLinkedList();
    public static final Map<ImmutableTuple<String, String>, Inventory> CACHED_INVENTORIES = Maps.newLinkedHashMap();

    public static ItemStack COUNTER_ADD_ITEM = CustomSkull.getHead(CustomSkull.SkullType.QUARTZ_PLUS, "§8» §a+1");
    public static ItemStack COUNTER_REMOVE_ITEM = CustomSkull.getHead(CustomSkull.SkullType.QUARTZ_MINUS, "§8» §c-1");

    private static final Button EMPTY_BUTTON = new Button() {};
    private static final ItemStack GLASS = new ItemCreator().setMaterial(Material.STAINED_GLASS_PANE).setData(7).setName("§c ").build();


    /**
     * Object Fields
     **/
    private String title;
    private int rows;
    private InventoryType inventoryType;
    private Inventory inventory;
    private List<InventoryButton> buttons;
    private boolean fillInventory;
    private boolean cache;
    private Updateable updateable;
    private Closeable closeable;
    private InventoryBuilderType builderType;
    private Player forPlayer;

    /**
     * Constructor
     *
     * @param plugin = main class that extends JavaPlugin
     * @param title  = title of inventory
     * @param rows   = amount of rows
     */
    public InventoryBuilder(Plugin plugin, Player player, String title, int rows) {
        initalize(plugin);
        this.forPlayer = player;
        this.title = title;
        this.rows = rows;
        this.buttons = Lists.newCopyOnWriteArrayList();
        this.fillInventory = false;
        this.cache = false;
    }

    /**
     * Constructor
     *
     * @param plugin = main class that extends JavaPlugin
     * @param title  = title of inventory
     * @param inventoryType   = set the type of inventory
     */
    public InventoryBuilder(Plugin plugin, Player player, String title, InventoryType inventoryType) {
        initalize(plugin);
        this.forPlayer = player;
        this.title = title;
        this.rows = 1;
        this.inventoryType = inventoryType;
        this.buttons = Lists.newCopyOnWriteArrayList();
        this.fillInventory = false;
        this.cache = false;
    }


    /**
     * Constructor
     * For serialize purpose
     *
     * @param plugin = main class that extends JavaPlugin
     */
    public InventoryBuilder(Plugin plugin) {
        initalize(plugin);
    }

    /**
     * Constructor
     * For types
     *
     * @param plugin = main class that extends JavaPlugin
     */
    public InventoryBuilder(Plugin plugin, Player player, String title, int rows, InventoryBuilderType builderType) {
        initalize(plugin);
        this.forPlayer = player;
        this.builderType = builderType;
        this.title = title;
        this.rows = rows;
        this.buttons = Lists.newCopyOnWriteArrayList();
        this.fillInventory = false;
        this.cache = false;
        this.inventoryType = null;
    }

    /**
     * Add closeable
     *
     * @param closeable
     * @return
     */
    public InventoryBuilder addCloseable(Closeable closeable){
        this.closeable = closeable;
        return this;
    }


    /**
     * Add updateable
     *
     * @param updateable
     * @return
     */
    public InventoryBuilder addUpdateable(Updateable updateable) {
        this.updateable = updateable;
        return this;
    }

    /**
     * Set title of the inventory
     *
     * @param title
     * @return
     */
    public InventoryBuilder setTitle(String title) {
        this.title = title;
        return this;
    }

    /**
     * Set player of inventory
     *
     * @param player
     * @return
     */
    public InventoryBuilder setForPlayer(Player player) {
        this.forPlayer = player;
        return this;
    }

    /**
     * Set rows of inventory
     *
     * @param rows
     * @return
     */
    public InventoryBuilder setRows(int rows) {
        this.rows = rows;
        return this;
    }

    /**
     * Add Empty button
     *
     * @param slot
     * @param itemStack
     * @return
     */
    public InventoryBuilder addEmptyButton(int slot, ItemStack itemStack) {
        buttons.add(new InventoryButton(slot, itemStack, EMPTY_BUTTON));
        return this;
    }


    /**
     * Update button by slot
     *
     * @param slot
     * @param itemStack
     * @return
     */
    public InventoryBuilder updateButton(int slot, ItemStack itemStack) {
        InventoryButton button = getButtonBySlot(slot);
        button.setItem(itemStack);

        if (getInventory() != null) {
            getInventory().setItem(button.getSlot(), itemStack);
        }
        return this;
    }


    /**
     * Get button by slot
     *
     * @param slot
     * @return
     */
    public InventoryButton getButtonBySlot(int slot) {
        for (InventoryButton button : getButtons()) {
            if (button.getSlot() == slot) {
                return button;
            }
        }
        return null;
    }




    /**
     * Fill inventory with glass
     *
     * @return
     */
    public InventoryBuilder fillInventory() {
        this.fillInventory = true;
        return this;
    }



    /**
     * Cache inventory
     *
     * @return
     */
    public InventoryBuilder cache() {
        this.cache = true;
        return this;
    }

    /**
     * Add a button
     *
     * @param button
     * @return
     */
    public InventoryBuilder addButton(InventoryButton button) {
        if (!buttons.contains(button)) {
            buttons.add(button);
        }
        return this;
    }


    /**
     * Rebuild buttons
     *
     * @return
     */
    public InventoryBuilder rebuildButtons() {
        for (InventoryButton button : getButtons()) {
            getInventory().setItem(button.getSlot(), button.getItem());
        }
        return this;
    }

    /**
     * Clear buttons matching the displayname
     * If displayname is null it removes all buttons
     *
     * @param matchedDisplayname
     */
    public void clearButtons(String matchedDisplayname) {
        if (matchedDisplayname == null) {
            return;
        }
        for (InventoryButton button : this.buttons) {
            if (button.getItem().getItemMeta().getDisplayName().equals(matchedDisplayname)) {
                this.buttons.remove(button);
            }
        }
    }

    /**
     * Clear all buttons
     */
    public void clearAllButtons() {
        this.buttons.clear();
    }


    /**
     * Remove button
     *
     * @param button
     * @return
     */
    public InventoryBuilder removeButton(InventoryButton button) {
        if (buttons.contains(button)) {
            buttons.remove(button);
        }
        return this;
    }

    /**
     * Build inventory
     *
     * @return
     */
    public InventoryBuilder build() {
        if (this.cache) {
            if (contains(getTitle(), getForPlayer().getName())) {
                this.inventory = getValue(getTitle(), getForPlayer().getName());
            } else {
                boolean containsType = this.inventoryType != null;
                if(containsType) {
                    this.inventory = Bukkit.createInventory(null, this.inventoryType, getTitle());
                } else {
                    this.inventory = Bukkit.createInventory(null, (9 * this.rows), getTitle());
                }

                ImmutableTuple<String, String> tuple = new ImmutableTuple<>(getForPlayer().getDisplayName(), getTitle());
                CACHED_INVENTORIES.put(tuple, inventory);
            }
        } else {
            boolean containsType = this.inventoryType != null;
            if(containsType) {
                this.inventory = Bukkit.createInventory(null, this.inventoryType, getTitle());
            } else {
                this.inventory = Bukkit.createInventory(null, (9 * this.rows), getTitle());
            }
        }
        InventoryBuilder.BUILDERS.add(this);
        if(!FIXED_TITLES.contains(getTitle())) {
            FIXED_TITLES.add(getTitle());
        }

        if (this.fillInventory) {
            for (int i = 0; i < inventory.getSize(); i++) {
                inventory.setItem(i, GLASS);
            }
        }
        if (getBuilderType() != null) {
            getBuilderType().setContents(this);
        }
        for (InventoryButton button : this.buttons) {
            inventory.setItem(button.getSlot(), button.getItem().clone());
        }

        return this;
    }

    /**
     * Check if a tuple contains in map
     * @param title
     * @param playerName
     * @return
     */
    private boolean contains(String title, String playerName){
        for(ImmutableTuple<String, String> tuple : CACHED_INVENTORIES.keySet()){
            if(tuple.getFirst().equals(playerName) && tuple.getSecond().equals(title)){
                return true;
            }
        }
        return false;
    }

    /**
     * Get tuple value
     * @param title
     * @param playerName
     * @return
     */
    private Inventory getValue(String title, String playerName){
        for(ImmutableTuple<String, String> tuple : CACHED_INVENTORIES.keySet()){
            if(tuple.getFirst().equals(playerName) && tuple.getSecond().equals(title)){
                return CACHED_INVENTORIES.get(tuple);
            }
        }
        return null;
    }

    /**
     * Open inventory
     *
     * @param player
     * @param sound
     */
    public void openInventory(Player player, Sound sound) {
        player.getPlayer().openInventory(this.inventory);
        if (sound != null) {
            player.playSound(player.getLocation(), sound, 1f, 1f);
        }
    }

    /**
     * Open inventory with animation
     *
     * @param player
     * @param sound
     */
    public void openInventoryAnimation(Player player, Sound sound, AnimationInventory.AnimationType type) {
        AnimationInventory animationInventory = new AnimationInventory(type, this.inventory);
        animationInventory.createAnimation(player);
        if (sound != null) {
            player.playSound(player.getLocation(), sound, 1f, 1f);
        }
    }

    public void openSubInventory(InventoryBuilder sub, Sound sound){
        if(getCloseable() != null) {
            getCloseable().onClose(getForPlayer());
        }
        BUILDERS.remove(this);
        sub.openInventory(getForPlayer(), sound);
    }

    /**
     * Clone inventory
     *
     * @return
     */
    @Override
    @SneakyThrows
    protected InventoryBuilder clone() {
        return (InventoryBuilder) super.clone();
    }

}

class InventoryBuilderListener implements Listener {

    /**
     * Default bukkit inventory click listener
     *
     * @param event
     */
    @EventHandler(priority = EventPriority.HIGHEST)
    public void onClick(InventoryClickEvent event) {
        for(String title : InventoryBuilder.FIXED_TITLES){
            if(event.getInventory() != null){
                if(event.getInventory().getTitle().equals(title)){
                    event.setResult(Event.Result.DENY);
                    event.setCancelled(true);
                }
            }
        }
        if (event.getCurrentItem() != null) {
            for (InventoryBuilder builder : InventoryBuilder.BUILDERS) {
                if (event.getInventory().getTitle().equals(builder.getTitle())) {
                    event.setResult(Event.Result.DENY);
                    event.setCancelled(true);
                    Player player = (Player) event.getWhoClicked();
                    if (builder.getForPlayer().getName().equals(player.getName())) {
                        if (event.getCurrentItem().hasItemMeta()) {
                            if (event.getCurrentItem().getItemMeta().hasDisplayName()) {
                                for (InventoryButton button : builder.getButtons()) {
                                    if (button.getButton() instanceof ClickButton) {
                                        if (event.getRawSlot() == button.getSlot()) {
                                            ((ClickButton) button.getButton()).onClick(player, event);
                                            return;
                                        }
                                    }
                                }
                            }
                        }
                        for (InventoryButton button : builder.getButtons()) {
                            if (button.getButton() instanceof ClickButton) {
                                if (event.getRawSlot() == button.getSlot()) {
                                    ((ClickButton) button.getButton()).onClick(player, event);
                                    return;
                                }
                            }
                        }
                    }

                }
            }
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onCall(InventoryDragEvent event){
        if (event.getInventory() != null) {
            for (InventoryBuilder builder : InventoryBuilder.BUILDERS) {
                if (event.getInventory().getTitle().equals(builder.getTitle())) {
                    event.setCancelled(true);
                }
            }
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onMove(InventoryMoveItemEvent event){
        if (event.getDestination() != null) {
            for (InventoryBuilder builder : InventoryBuilder.BUILDERS) {
                if (event.getDestination().getTitle().equals(builder.getTitle())) {
                    event.setCancelled(true);
                }
            }
        }
    }

    /**
     * Default bukkit inventory close listener
     *
     * @param event
     */
    @EventHandler
    public void onClose(InventoryCloseEvent event) {
        for (InventoryBuilder builder : InventoryBuilder.BUILDERS) {
            if (builder.getTitle().equals(event.getInventory().getTitle())) {
                if (builder.getForPlayer() != null) {
                    Player player = (Player) event.getPlayer();
                    if (builder.getForPlayer().getName().equals(player.getName())) {
                        if(builder.getCloseable() != null) {
                            builder.getCloseable().onClose(player);
                        }
                        InventoryBuilder.BUILDERS.remove(builder);
                    }
                }

            }
        }
    }
}

class InventoryBuilderTask extends BukkitRunnable {

    @Override
    public void run() {
        for (InventoryBuilder builder : InventoryBuilder.BUILDERS) {
            if (builder.getUpdateable() != null) {
                Updateable updateable = builder.getUpdateable();
                updateable.onUpdate(builder.getForPlayer(), builder);
            }
        }
    }
}
