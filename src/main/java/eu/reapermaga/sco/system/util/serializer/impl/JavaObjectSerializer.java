package eu.reapermaga.sco.system.util.serializer.impl;

import eu.reapermaga.sco.system.util.serializer.Serializer;
import lombok.Cleanup;
import lombok.SneakyThrows;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Base64;

/**
 * Copyright (c) ReaperMaga, All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by ReaperMaga, 19.09.18
 **/
public final class JavaObjectSerializer implements Serializer<String, Object> {

    @Override
    @SneakyThrows
    public String serialize(Object object) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        @Cleanup ObjectOutputStream outputStream = new ObjectOutputStream(byteArrayOutputStream);
        outputStream.writeObject( object );
        outputStream.close();
        return Base64.getEncoder().encodeToString(byteArrayOutputStream.toByteArray());
    }

    @Override
    @SneakyThrows
    public Object deserialize(String base64) {
        byte [] bytes = Base64.getDecoder().decode( base64 );
        @Cleanup ObjectInputStream objectInputStream = new ObjectInputStream(new ByteArrayInputStream(bytes));
        Object object  = objectInputStream.readObject();
        return object;
    }
}
