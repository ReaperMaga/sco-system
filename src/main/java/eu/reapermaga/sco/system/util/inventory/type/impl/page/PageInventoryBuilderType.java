package eu.reapermaga.sco.system.util.inventory.type.impl.page;

import com.google.common.collect.Lists;
import eu.reapermaga.sco.system.util.inventory.InventoryBuilder;
import eu.reapermaga.sco.system.util.inventory.buttons.InventoryButton;
import eu.reapermaga.sco.system.util.inventory.buttons.impl.ClickButton;
import eu.reapermaga.sco.system.util.inventory.skull.CustomSkull;
import eu.reapermaga.sco.system.util.inventory.type.InventoryBuilderType;
import eu.reapermaga.sco.system.util.inventory.util.ItemCreator;
import lombok.Getter;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;

import java.util.List;

/**
 * Copyright (c) ReaperMaga, All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by ReaperMaga, 22.09.18
 **/
@Getter
public class PageInventoryBuilderType implements InventoryBuilderType {

    private static final String SITE_KEY = "siteDisplay";
    private static final String ITEM_KEY = "item";
    private static final String PREVIOUS_KEY = "previousItem";

    private static final ItemStack NEXT_ITEM = CustomSkull.getHead(CustomSkull.SkullType.ARROW_RIGHT, "§8» §eNext");
    private static final ItemStack PREVIOUS_ITEM = CustomSkull.getHead(CustomSkull.SkullType.ARROW_LEFT, "§8» §ePrevious");
    private static final ItemStack PREVIOUS_ITEM_NULL = new ItemCreator().setMaterial(Material.BARRIER).setName("§8» §cPrevious").build();

    private int currentPage;
    private int siteSlot;
    private int[] slots;
    private List<ItemStack> items;
    private PageAction action;
    private int slotLength;

    private int nextSiteSlot;
    private int previousSiteSlot;

    /**
     * Constructor
     * @param siteSlot
     * @param slots
     * @param items
     * @param action
     */
    public PageInventoryBuilderType(int siteSlot, int[] slots, List<ItemStack> items, PageAction action) {
        this.siteSlot = siteSlot;
        this.slots = slots;
        this.currentPage = 1;
        this.items = items;
        this.action = action;
        this.slotLength = slots.length;

    }

    /**
     * Constructor
     * @param siteSlot
     * @param slots
     * @param items
     * @param action
     */
    public PageInventoryBuilderType(int siteSlot, int nextSiteSlot, int previousSiteSlot, int[] slots, List<ItemStack> items, PageAction action) {
        this.siteSlot = siteSlot;
        this.slots = slots;
        this.currentPage = 1;
        this.items = items;
        this.action = action;
        this.slotLength = slots.length;
        this.nextSiteSlot = nextSiteSlot;
        this.previousSiteSlot = previousSiteSlot;

    }

    /**
     * Set contents for builder
     * @param builder
     */
    @Override
    public void setContents(InventoryBuilder builder) {
        int rows = builder.getRows();
        int nextSlot = ((9 * rows)-1);
        int previousSlot = ((9 * rows)-1-8);

        if(nextSiteSlot == 0 && previousSiteSlot == 0){
            this.nextSiteSlot = nextSlot;
            this.previousSiteSlot = previousSlot;
        }


        builder.addButton(new InventoryButton(nextSiteSlot, NEXT_ITEM, new ClickButton() {
            @Override
            public void onClick(Player player, InventoryClickEvent event) {
                openNextPage(builder);
            }
        }));

        builder.addButton(new InventoryButton(previousSiteSlot, PREVIOUS_ITEM_NULL, new ClickButton() {
            @Override
            public void onClick(Player player, InventoryClickEvent event) {
                if(event.getCurrentItem().getItemMeta().getDisplayName().equals(PREVIOUS_ITEM_NULL.getItemMeta().getDisplayName())){
                    return;
                }
                openPreviousPage(builder);
            }
        }));

        builder.addEmptyButton(getSiteSlot(), new ItemCreator().setMaterial(Material.EYE_OF_ENDER).setName("§8» §eAktuelle Seite§8: §7" + currentPage).build());

        updateItems(builder, true);
    }


    /**
     * Update all items
     *
     * @param builder
     * @param newItemStacks
     */
    public void updateAllItems(InventoryBuilder builder, List<ItemStack> newItemStacks) {
        this.items = newItemStacks;
        if(currentPage == 1) {
            updateItems(builder, true);
        } else {
            updateItems(builder, false);
        }
    }


    /**
     * Open next page
     * @param builder
     */
    public void openNextPage(InventoryBuilder builder){
        final int size = getItems().size();
        final int full = currentPage * getSlotLength();
        if(size >= full){
            this.currentPage = currentPage + 1;
            updateItems(builder, false);
        }
    }


    /**
     * Open previous page
     * @param builder
     */
    public void openPreviousPage(InventoryBuilder builder){
        if(currentPage <= 1){
            return;
        }
        this.currentPage = currentPage - 1;
        updateItems(builder, false);
    }

    /**
     * Update items for the currentPage
     * @param builder
     * @param firstOpen
     */
    public void updateItems(InventoryBuilder builder, boolean firstOpen){
        builder.updateButton(getSiteSlot(), new ItemCreator().setMaterial(Material.EYE_OF_ENDER).setName("§8» §eAktuelle Seite§8: §7" + currentPage).build());
        if(currentPage >= 2){
            builder.updateButton(getPreviousSiteSlot(), PREVIOUS_ITEM);
        } else if(currentPage <= 1){
            builder.updateButton(getPreviousSiteSlot(), PREVIOUS_ITEM_NULL);
        }
        clearSlots(builder);

        List<ItemStack> items = currentPage == 1 ? loadItems(0, getSlotLength()) : loadItems(((currentPage-1)*getSlotLength()), currentPage * getSlotLength());


        int currentSlot = 0;
        for(ItemStack item : items){
            if(firstOpen){
                builder.addButton(new InventoryButton(getSlots()[currentSlot], item, new ClickButton() {
                    @Override
                    public void onClick(Player player, InventoryClickEvent event) {
                        getAction().onClickItem(player, event);
                    }
                }));
            } else {
                builder.updateButton(getSlots()[currentSlot], item);
            }
            currentSlot++;
        }
    }

    /**
     * Clear slots
     */
    private void clearSlots(InventoryBuilder builder){
        if(builder.getInventory() != null){
            for(int slot : getSlots()){
                builder.getInventory().setItem(slot, null);
            }
        }
    }

    /**
     * Load items from a position
     * Example: 0-18 -> the return list you get is only the contents from 0-18 of the objects list
     * @param from
     * @param to
     * @return
     */
    private List<ItemStack> loadItems(int from, int to){
        List<ItemStack> newList = Lists.newArrayList();
        List<ItemStack> currentList = getItems();
        for (int i = from; i < to; i++) {
            if(currentList.size() > i){
                if(currentList.get(i) != null){
                    newList.add(currentList.get(i));
                }
            }
        }
        return newList;
    }
}
