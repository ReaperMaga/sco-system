package eu.reapermaga.sco.system.util.math.location;

public class Location3D {


	double x;
	double y;
	double z;

	public Location3D(double x, double y, double z) {
		this.x = x;
		this.y = y;
		this.z = z;
	}

	public double getX() {
		return this.x;
	}

	public double getY() {
		return this.y;
	}

	public double getZ() {
		return this.z;
	}

	public double distance(Location3D Location) {
		double a = Math.abs(Location.getY() - this.y);

		double ba = Math.abs(Location.getZ() - this.z);
		double bb = Math.abs(Location.getX() - this.x);
		double b = Math.sqrt(ba * ba + bb * bb);

		double c = Math.sqrt(a * a + b * b);
		return c;
	}

	public String toPointString() {
		return "(" + this.x + " | " + this.y + " | " + this.z + ")";
	}
}