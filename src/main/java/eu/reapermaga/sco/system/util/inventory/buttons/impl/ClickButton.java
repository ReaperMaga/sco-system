package eu.reapermaga.sco.system.util.inventory.buttons.impl;

import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;

/**
 * Copyright (c) ReaperMaga, All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by ReaperMaga, 22.08.2018
 **/
public interface ClickButton extends Button {


    void onClick(Player player, InventoryClickEvent event);
}
