package eu.reapermaga.sco.system.util.inventory.extras;


import eu.reapermaga.sco.system.util.inventory.util.ItemCreator;
import lombok.NoArgsConstructor;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.Plugin;

import java.util.*;

/**
 * Copyright (c) ReaperMaga, All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by ReaperMaga, 21.08.2018
 **/
@NoArgsConstructor
public class MenuInventory  {


    /**
     * Initalize
     */
    public static boolean INITALIZED = false;
    public static void initalize(Plugin plugin) {
        if (!INITALIZED) {
            INITALIZED = true;
            plugin.getServer().getPluginManager().registerEvents(new MenuListener(), plugin);
        }
    }



    private static MenuInventory service;

    /** Field constants **/
    public static final String INVENTORY_NAME = "§eBestätigung";
    public static final String CANCEL_PRODUCT = "§cDu hast die Bestätigung abgebrochen.";

    public static final ItemStack ACCEPT_ITEM = new ItemCreator().setMaterial(Material.WOOL).setData(5).setName("§aBestätigen").build();
    public static final ItemStack DENY_ITEM = new ItemCreator().setMaterial(Material.WOOL).setData(14).setName("§cAbbrechen").build();

    /** Map **/
    private Map<String, Menu> menus = new LinkedHashMap<>();


    /**
     * Create a Menu
     * @param plugin = main class that extends JavaPlugin
     * @param player
     * @param menuTransaction
     * @return
     */
    public Menu createMenu(Plugin plugin, Player player, MenuTransaction menuTransaction) {
        initalize(plugin);
        Menu product = new Menu(player, menuTransaction);
        getMenus().put(player.getName(), product);
        return product;
    }


    /**
     * Get menus
     * @return
     */
    public Map<String, Menu> getMenus() {
        return menus;
    }


    /**
     * Menu Object
     */
    public class Menu {

        /** Fields **/
        private Player player;
        private MenuTransaction menuTransaction;
        private Inventory inventory;

        /** Constructor **/
        public Menu(Player player, MenuTransaction menuTransaction) {
            this.player = player;
            this.menuTransaction = menuTransaction;
            createInventory();
        }


        /**
         * Create inventory
         */
        public void createInventory() {
            this.inventory = Bukkit.createInventory(null, 9, INVENTORY_NAME);

            for (int i = 0; i < getInventory().getSize(); i++) {
                this.getInventory().setItem(i, ItemCreator.NULL);
            }
            for (int i = 0; i < 3; i++) {
                this.getInventory().setItem(i, ACCEPT_ITEM);
            }
            for (int i = 6; i < 9; i++) {
                this.getInventory().setItem(i, DENY_ITEM);
            }

            List<String> lore = new ArrayList<>();
            lore.add("§c ");
            lore.add(" §8➥ §e" + getMenuTransaction().name());
            lore.add("§c ");
            this.getInventory().setItem(4, new ItemCreator().setMaterial(Material.PAPER).setName("§8» §bInformation").setLore(lore).addEnchantment(Enchantment.ARROW_INFINITE, 1)
                    .setItemFlags(Arrays.asList(ItemFlag.HIDE_ENCHANTS)).build());
            getPlayer().openInventory(this.getInventory());
        }

        public Inventory getInventory() {
            return inventory;
        }

        public MenuTransaction getMenuTransaction() {
            return menuTransaction;
        }

        public Player getPlayer() {
            return player;
        }
    }

    public interface MenuTransaction {

        String name();

        void onAction(Menu menu);

    }

    public static MenuInventory getService() {
        if (service == null) {
            service = new MenuInventory();
            return service;
        }
        return service;
    }

}

class MenuListener implements Listener {

    /**
     * Default bukkit click listener
     * @param event
     */
    @EventHandler
    public void onClick(InventoryClickEvent event) {
        if (event.getClickedInventory() != null) {
            if (event.getCurrentItem() != null) {
                if (event.getCurrentItem().hasItemMeta()) {
                    if (event.getCurrentItem().getItemMeta().hasDisplayName()) {
                        if (event.getInventory().getTitle().equals(MenuInventory.INVENTORY_NAME)) {
                            event.setCancelled(true);
                            event.setResult(Event.Result.DENY);
                            Player player = (Player) event.getWhoClicked();
                            if (!MenuInventory.getService().getMenus().containsKey(player.getName())) {
                                return;
                            }
                            MenuInventory.Menu menu = MenuInventory.getService().getMenus().get(player.getName());
                            if (event.getCurrentItem().getItemMeta().getDisplayName().equals(MenuInventory.ACCEPT_ITEM.getItemMeta().getDisplayName())) {
                                menu.getMenuTransaction().onAction(menu);
                                player.playSound(player.getLocation(), Sound.ENTITY_PLAYER_LEVELUP, 1f, 1f);
                                event.getView().close();
                                MenuInventory.getService().getMenus().remove(player.getName());
                                return;
                            }
                            if (event.getCurrentItem().getItemMeta().getDisplayName().equals(MenuInventory.DENY_ITEM.getItemMeta().getDisplayName())) {
                                event.getView().close();
                                MenuInventory.getService().getMenus().remove(player.getName());
                                player.playSound(player.getLocation(), Sound.ENTITY_CAT_HISS, 1f, 1f);
                                return;
                            }
                        }
                    }
                }
            }
        }
    }

    /**
     * Default bukkit inventory close listener
     * @param event
     */
    @EventHandler
    public void onClose(InventoryCloseEvent event) {
        if (event.getInventory() != null) {
            if (!event.getInventory().getTitle().equals(MenuInventory.INVENTORY_NAME)) {
                return;
            }
            Player player = (Player) event.getPlayer();
            if (MenuInventory.getService().getMenus().containsKey(player.getName())) {
                MenuInventory.getService().getMenus().remove(player.getName());
            }
        }
    }


}
