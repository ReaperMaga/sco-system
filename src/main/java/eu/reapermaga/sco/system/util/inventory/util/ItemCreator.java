package eu.reapermaga.sco.system.util.inventory.util;

import lombok.Getter;
import org.bukkit.Color;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.LeatherArmorMeta;
import org.bukkit.inventory.meta.SkullMeta;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Copyright (c) ReaperMaga, All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by ReaperMaga, 12.07.2018
 **/
@Getter
public class ItemCreator {

    public static ItemStack NULL = new ItemCreator().spacer().build();

    private Material material;
    private int amount = 1, data = 0;
    private Color color;
    private List<String> lore;
    private List<ItemFlag> itemFlags;
    private String name;
    private Map<Enchantment, Integer> enchantments;
    private String skullOwner;

    /**
     * Constructor
     * Initalize default fields
     */
    public ItemCreator() {
        this.material = Material.BARRIER;
        this.amount = 1;
        this.data = 0;
        this.name = null;
        this.color = null;
        this.skullOwner = null;
        this.enchantments = new HashMap<>();
        this.lore = new ArrayList<>();
        this.itemFlags = new ArrayList<>();
    }

    /**
     * Set skull owner
     * @param owner
     * @return
     */
    public ItemCreator setSkull(String owner) {
        this.skullOwner = owner;
        return this;
    }

    /**
     * Add a item flag
     * @param flag = bukkit ItemFlag
     * @return
     */
    public ItemCreator addFlag(ItemFlag flag) {
        if (itemFlags != null) {
            itemFlags.add(flag);
        }
        return this;
    }

    /**
     * Add a lore
     * @param string = lore
     * @return
     */
    public ItemCreator addLore(String string) {
        if (lore != null) {
            lore.add(string);
        }
        return this;
    }

    /**
     * Add enchantment with a level
     * @param enchantment
     * @param level
     * @return
     */
    public ItemCreator addEnchantment(Enchantment enchantment, int level) {
        if (enchantments != null) {
            enchantments.put(enchantment, level);
        }
        return this;
    }

    /**
     * Set color if the item can be colored
     * @param color
     * @return
     */
    public ItemCreator setColor(Color color) {
        this.color = color;
        return this;
    }

    /**
     * Set the amount of items
     * @param amount
     * @return
     */
    public ItemCreator setAmount(int amount) {
        this.amount = amount;
        return this;
    }

    /**
     * Set the short id
     * @param data = shortid
     * @return
     */
    public ItemCreator setData(int data) {
        this.data = data;
        return this;
    }

    /**
     * Make the item glow
     * Adding itemflag
     * @return
     */
    public ItemCreator glow() {
        this.itemFlags.add(ItemFlag.HIDE_ENCHANTS);
        this.addEnchantment(Enchantment.DEPTH_STRIDER, 1);
        return this;
    }

    /**
     * Set enchantments
     * @param enchantments
     * @return
     */
    public ItemCreator setEnchantments(Map<Enchantment, Integer> enchantments) {
        this.enchantments = enchantments;
        return this;
    }

    /**
     * Set ItemFlags
     * @param itemFlags
     * @return
     */
    public ItemCreator setItemFlags(List<ItemFlag> itemFlags) {
        this.itemFlags = itemFlags;
        return this;
    }

    /**
     * Set lore
     * @param lore
     * @return
     */
    public ItemCreator setLore(List<String> lore) {
        this.lore = lore;
        return this;
    }

    /**
     * Set material
     * @param material
     * @return
     */
    public ItemCreator setMaterial(Material material) {
        this.material = material;
        return this;
    }

    /**
     * Set DisplayName
     * @param name = displayName
     * @return
     */
    public ItemCreator setName(String name) {
        this.name = name;
        return this;
    }


    /**
     * Build item
     * @return = itemstack
     */
    public ItemStack build() {
        if (material == null) {
            return null;
        }
        ItemStack itemStack = new ItemStack(material, amount, (short) data);

        if (material == Material.SKULL_ITEM) {
            if(getSkullOwner() != null){
                SkullMeta currentMeta = (SkullMeta)itemStack.getItemMeta();
                currentMeta.setOwner(getSkullOwner());
                itemStack.setItemMeta(currentMeta);
            }

        }

        ItemMeta itemMeta = itemStack.getItemMeta();

        if (itemFlags != null) {
            for (ItemFlag flag : itemFlags) {
                itemMeta.addItemFlags(flag);
            }
        }

        if (lore != null) {
            itemMeta.setLore(lore);
        }

        if (name != null) {
            itemMeta.setDisplayName(name);
        }


        if (itemMeta instanceof LeatherArmorMeta && color != null) {
            ((LeatherArmorMeta) itemMeta).setColor(color);
        }

        if (enchantments != null) {
            for (Enchantment enchantment : enchantments.keySet()) {
                itemMeta.addEnchant(enchantment, enchantments.get(enchantment), true);
            }
        }

        itemStack.setItemMeta(itemMeta);

        return itemStack;
    }

    public ItemCreator spacer() {
        this.material = Material.STAINED_GLASS_PANE;
        this.data = 7;
        this.amount = 1;
        this.name = "§r";
        return this;
    }
}