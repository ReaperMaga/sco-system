package eu.reapermaga.sco.system.util.time;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

/**
 * Copyright (c) ReaperMaga, All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by ReaperMaga
 **/
public class TimeUtils {

    private static final int MINUTES_IN_AN_HOUR = 60;
    private static final int SECONDS_IN_A_MINUTE = 60;

    private static final DateFormat FORMAT = new SimpleDateFormat("HH 'Std.', mm 'Min.'");



    /**
     * Get hours and minutes in digits
     *
     * @param totalSeconds
     * @return
     */
    public static String timeConversion(int totalSeconds) {
        int hours = totalSeconds / MINUTES_IN_AN_HOUR / SECONDS_IN_A_MINUTE;
        int minutes = (totalSeconds - (hoursToSeconds(hours)))
                / SECONDS_IN_A_MINUTE;
        int seconds = totalSeconds
                - ((hoursToSeconds(hours)) + (minutesToSeconds(minutes)));

        return twoDigitString(hours) + ":" + twoDigitString(minutes);
    }

    private static int hoursToSeconds(int hours) {
        return hours * MINUTES_IN_AN_HOUR * SECONDS_IN_A_MINUTE;
    }

    private static int minutesToSeconds(int minutes) {
        return minutes * SECONDS_IN_A_MINUTE;
    }



    public static String getTime(long time){
        String message = "";

        int seconds = (int) (time / 1000);

        if(seconds >= 60*60*24){
            int days = seconds / (60*60*24);
            seconds = seconds % (60*60*24);

            final String convert = days == 1 ? " Tag " : " Tage ";
            message += days + convert;
        }
        if(seconds >= 60*60){
            int hours = seconds / (60*60);
            seconds = seconds % (60*60);

            final String convert = hours == 1 ? " Stunde " : " Stunden ";
            message += hours + convert;
        }
        if(seconds >= 60){
            int min = seconds / 60;
            seconds = seconds % 60;

            final String convert = min == 1 ? " Minute " : " Minuten ";

            message += min + convert;
        }
        if(seconds >= 0){
            final String convert = seconds == 1 ? " Sekunde " : " Sekunden ";

            message += seconds + convert;
        }

        return message;
    }

    public static String getDividedTime(long time){
        String message = "";

        long now = System.currentTimeMillis();
        long diff = time - now;

        int seconds = (int) (diff / 1000);

        if(seconds >= 60*60*24){
            int days = seconds / (60*60*24);
            seconds = seconds % (60*60*24);

            final String convert = days == 1 ? " Tag " : " Tage ";
            message += days + convert;
        }
        if(seconds >= 60*60){
            int hours = seconds / (60*60);
            seconds = seconds % (60*60);

            final String convert = hours == 1 ? " Stunde " : " Stunden ";
            message += hours + convert;
        }
        if(seconds >= 60){
            int min = seconds / 60;
            seconds = seconds % 60;

            final String convert = min == 1 ? " Minute " : " Minuten ";

            message += min + convert;
        }
        if(seconds >= 0){
            final String convert = seconds == 1 ? " Sekunde " : " Sekunden ";

            message += seconds + convert;
        }

        return message;
    }

    public static String getDividedTimeShort(long time){
        String message = "";

        long now = System.currentTimeMillis();
        long diff = time - now;

        int seconds = (int) (diff / 1000);

        if(seconds >= 60*60){
            int hours = seconds / (60*60);
            seconds = seconds % (60*60);

            final String convert = ":";
            message += hours + convert;
        }
        if(seconds >= 60){
            int min = seconds / 60;
            seconds = seconds % 60;

            final String convert = ":";

            message += min + convert;
        }
        if(seconds >= 0){
            message += seconds;
        }

        return message;
    }



    /**
     * Duration to string in two digits(hour)
     * @param seconds
     * @return
     */
    public static String getDurationStringHourAndMinute(int seconds) {
        int hours = seconds / 3600;
        int minutes = seconds % 60;
        return twoDigitString(hours) + ":" + twoDigitString(minutes);
    }

    public static String getFormatedTime(long millis) {
        int h = (int) ((millis / 1000) / 3600);
        int m = (int) (((millis / 1000) / 60) % 60);
        return h + ":" + m;
    }

    /**
     * Split number in two digits
     * @param number
     * @return
     */
    public static String twoDigitString(int number) {

        if (number == 0) {
            return "00";
        }

        if (number / 10 == 0) {
            return "0" + number;
        }

        return String.valueOf(number);
    }
}
