package eu.reapermaga.sco.system.util.inventory.animation;

import eu.reapermaga.sco.system.ScoPlugin;
import lombok.Getter;
import lombok.Setter;
import org.bukkit.Bukkit;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scheduler.BukkitTask;

import java.util.HashMap;
import java.util.Map;

/**
 * Copyright (c) ReaperMaga, All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by ReaperMaga, 22.08.2018
 **/
public class AnimationInventory {


    @Getter private BukkitTask task;
    @Getter @Setter private Inventory inventory;
    @Getter private int offset = 0;
    @Getter @Setter private Inventory animation;
    @Getter @Setter private AnimationType type;
    private int rows;


    public AnimationInventory(AnimationType type, Inventory inventory) {
        setInventory(inventory);
        setAnimation(Bukkit.createInventory(null, inventory.getSize(), inventory.getTitle()));
        rows = getInventory().getSize()/9;
        setType(type);
    }

    public void createAnimation(Player player){
        player.openInventory(getAnimation());
        player.playSound(player.getLocation(), Sound.UI_BUTTON_CLICK, 1f, 1f);
        task = new BukkitRunnable() {
            @Override
            public void run() {
                shift();
                setItems(offset);
                if(offset++ == 8){
                    stop();
                    player.playSound(player.getLocation(), Sound.UI_BUTTON_CLICK, 1f, 1f);
                    return;
                }

            }
        }.runTaskTimerAsynchronously(ScoPlugin.getInstance(), 0L, 2L);
    }

    public void shift(){
        Map<Integer, ItemStack> contents = getContents();
        contents.keySet().forEach((slot)->{
            ItemStack stack = contents.get(slot);
            switch (type){
                case RIGHT:
                    getAnimation().setItem(slot+1, stack);
                    break;
                case LEFT:
                    getAnimation().setItem(slot-1, stack);
                    break;
            }
        });
    }


    
    public void setItems(int offset){
        Map<Integer, ItemStack> column = null;
        switch (type){
            case RIGHT:
                column = getColumn(8-offset);
                break;
            case LEFT:
                column = getColumn(offset);
                break;
        }
        for(int slot : column.keySet()){
            getAnimation().setItem(slot, column.get(slot));
        }
    }


    
    public Map<Integer, ItemStack> getColumn(int index){
        Map<Integer, ItemStack> map = new HashMap<>();
        AnimationType type = getType();
        for (int i = 0; i < rows; i++) {
            int value = i*9+index;
            ItemStack stack = inventory.getItem(value);
            if(stack!= null) {
                if (stack.hasItemMeta()) {
                    if (stack.getItemMeta().hasDisplayName()) {
                        switch (type){
                            case RIGHT:
                                map.put(i*9, getInventory().getItem(value));
                                break;
                            case LEFT:
                                map.put(i*9+8, getInventory().getItem(value));
                                break;
                        }

                    }
                }
            }

        }
        return map;
    }




    public Map<Integer, ItemStack> getContents(){
        Map<Integer, ItemStack> map = new HashMap<>();
        for (int i = 0; i < getAnimation().getSize(); i++) {
            ItemStack stack = getAnimation().getItem(i);
            if(stack!= null) {
                if (stack.hasItemMeta()) {
                    if (stack.getItemMeta().hasDisplayName()) {
                         map.put(i, getAnimation().getItem(i));

                }
            }
            }

        }
        return map;
    }



    public void stop(){
        task.cancel();
        task = null;
    }

    public enum AnimationType {
        RIGHT,
        LEFT;
    }
}

