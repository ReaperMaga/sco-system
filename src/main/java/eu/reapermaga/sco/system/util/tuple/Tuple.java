package eu.reapermaga.sco.system.util.tuple;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * Copyright (c) ReaperMaga, All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by ReaperMaga
 **/
@Getter
@Setter
@AllArgsConstructor
public class Tuple<T, K> implements Serializable {

    private T first;
    private K second;
}
