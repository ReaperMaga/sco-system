package eu.reapermaga.sco.system.util.inventory.type.impl.page;

import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;

/**
 * Copyright (c) ReaperMaga, All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by ReaperMaga, 22.09.18
 **/
public interface PageAction {

    void onClickItem(Player player, InventoryClickEvent event);
}
