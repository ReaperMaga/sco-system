package eu.reapermaga.sco.system.util.inventory.buttons;


import eu.reapermaga.sco.system.util.inventory.buttons.impl.Button;
import lombok.Getter;
import lombok.Setter;
import org.bukkit.inventory.ItemStack;

/**
 * Copyright (c) ReaperMaga, All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by ReaperMaga, 22.08.2018
 **/
@Getter
public class InventoryButton {

    private final int slot;
    @Setter
    private ItemStack item;
    private final Button button;

    public InventoryButton(int slot, ItemStack item, Button button) {
        this.slot = slot;
        this.item = item;
        this.button = button;
    }

}
