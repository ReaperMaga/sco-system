package eu.reapermaga.sco.system.util.inventory.skull;

/**
 * Erstellt von ItsMaga
 */

import com.mojang.authlib.GameProfile;
import com.mojang.authlib.properties.Property;
import com.mojang.authlib.properties.PropertyMap;
import org.apache.commons.codec.binary.Base64;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.Arrays;
import java.util.List;
import java.util.UUID;

public class CustomSkull {

    /**
     * Get head by SkullType
     * @param type
     * @return
     */
    public static ItemStack getHead(SkullType type) {
        return getSkull(type.getURL());
    }

    /**
     * Get head by SkullType with displayname
     * @param type
     * @param displayname
     * @return
     */
    public static ItemStack getHead(SkullType type, String displayname) {
        ItemStack stack = CustomSkull.getHead(type);
        ItemMeta meta = stack.getItemMeta();
        meta.setDisplayName(displayname);
        stack.setItemMeta(meta);
        return stack;
    }


    /**
     * Get head by SkullType with displayname and lore
     * @param type
     * @param displayname
     * @param lore
     * @return
     */
    public static ItemStack getHead(SkullType type, String displayname, List<String> lore) {
        ItemStack stack = CustomSkull.getHead(type);
        ItemMeta meta = stack.getItemMeta();
        meta.setDisplayName(displayname);
        meta.setLore(lore);
        stack.setItemMeta(meta);
        return stack;
    }

    /**
     * Get head by url with displayname and lore
     * @param url
     * @param displayName
     * @param lore
     * @return
     */
    public static ItemStack getSkull(String url, String displayName, String... lore) {
        ItemStack head = getSkull(url);
        ItemMeta meta = head.getItemMeta();
        meta.setDisplayName(displayName);
        meta.setLore(Arrays.asList(lore));
        head.setItemMeta(meta);
        return head;
    }

    /**
     * Get head by url with displayname
     * @param url
     * @param displayName
     * @return
     */
    public static ItemStack getSkull(String url, String displayName) {
        ItemStack head = getSkull(url);
        ItemMeta meta = head.getItemMeta();
        meta.setDisplayName(displayName);
        head.setItemMeta(meta);
        return head;
    }

    /**
     * Get head by url
     * @param url
     * @return
     */
    public static ItemStack getSkull(String url) {
        GameProfile profile = new GameProfile(UUID.randomUUID(), null);
        PropertyMap propertyMap = profile.getProperties();
        if (propertyMap == null) {
            throw new IllegalStateException("Profile doesn't contain a property map");
        }
        byte[] encodedData = Base64.encodeBase64(String.format("{textures:{SKIN:{url:\"%s\"}}}", url).getBytes());
        propertyMap.put("textures", new Property("textures", new String(encodedData)));
        ItemStack head = new ItemStack(Material.SKULL_ITEM, 1, (short) 3);
        ItemMeta headMeta = head.getItemMeta();
        Class<?> headMetaClass = headMeta.getClass();
        Reflections.getField(headMetaClass, "profile", GameProfile.class).set(headMeta, profile);
        head.setItemMeta(headMeta);
        return head;
    }



    public enum SkullType {
        /**
         * Wood Arrows
         */
        ARROW_DOWN("http://textures.minecraft.net/texture/2dadd755d08537352bf7a93e3bb7dd4d733121d39f2fb67073cd471f561194dd"),
        ARROW_RIGHT("http://textures.minecraft.net/texture/1b6f1a25b6bc199946472aedb370522584ff6f4e83221e5946bd2e41b5ca13b"),
        ARROW_LEFT("http://textures.minecraft.net/texture/3ebf907494a935e955bfcadab81beafb90fb9be49c7026ba97d798d5f1a23"),
        ARROW_UP("http://textures.minecraft.net/texture/3040fe836a6c2fbd2c7a9c8ec6be5174fddf1ac20f55e366156fa5f712e10"),


        /**
         * Others
         */
        FOOTBALL("http://textures.minecraft.net/texture/f44881b6137ba9b810abb0da9a9ef88715d82b91c9b80f08fb15b7ba46ae273"),
        YOUTUBE("http://textures.minecraft.net/texture/375651a948aa2030a2d5b6c2fbde5d58c4b8a0c8c3f9e4c360f3f72e292e5113"),
        PICKAXE("http://textures.minecraft.net/texture/351afafc6f646dd9ff9c774a82ee89da70d3323722cd02cbcf3dc61ae717bbc"),
        /**
        /**
         * Quartz
         */
        QUARTZ_ARROW_RIGHT_DOWN("http://textures.minecraft.net/texture/3667d141c41b05b74bb146eec22b21c5fea3367efdb6c7b5bfb5ef8a9e34aa2"),
        QUARTZ_ARROW_RIGHT_UP("http://textures.minecraft.net/texture/aacacb836c5e428b49b5d224cab22828efe2f6c704f759364d71c656e301420"),

        QUARTZ_ARROW_LEFT_UP("http://textures.minecraft.net/texture/f2eeefd986ad106434c64fac669f70ef88dc6bb6c82cbd7fa8d66436973db1ad"),
        QUARTZ_ARROW_LEFT_DOWN("http://textures.minecraft.net/texture/a7dd76ab3f9c645196e2458564d8556ad59fd32725ed4888c01e307a37aeb"),
        QUARTZ_ALPHABET_S("http://textures.minecraft.net/texture/f38d2759569d515d2454d4a7891a94cc63ddfe72d03bfdf76f1d4277d590"),
        QUARTZ_PLUS("http://textures.minecraft.net/texture/47a0fc6dcf739c11fece43cdd184dea791cf757bf7bd91536fdbc96fa47acfb"),
        QUARTZ_MINUS("http://textures.minecraft.net/texture/9d6b1293db729d010f534ce1361bbc55ae5a8c8f83a1947afe7a86732efc2");



        String message;

        SkullType(String message) {
            this.message = message;
        }

        public String getURL() {
            return message;
        }
    }

}