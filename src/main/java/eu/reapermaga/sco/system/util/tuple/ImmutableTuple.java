package eu.reapermaga.sco.system.util.tuple;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * Copyright (c) ReaperMaga, All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by ReaperMaga
 **/
@Getter
@AllArgsConstructor
public class ImmutableTuple<T, K> {

    private final T first;
    private final K second;
}
