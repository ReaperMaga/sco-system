package eu.reapermaga.sco.system.util;

import com.google.common.util.concurrent.MoreExecutors;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Copyright (c) ReaperMaga, All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by ReaperMaga
 **/
public class AsyncDispatcher {

    public static final ExecutorService POOL = MoreExecutors.listeningDecorator(Executors.newCachedThreadPool());

    public static void dispatch(Runnable runnable){
        POOL.execute(runnable);
    }
}
