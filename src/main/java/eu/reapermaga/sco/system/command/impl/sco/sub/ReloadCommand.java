package eu.reapermaga.sco.system.command.impl.sco.sub;

import eu.reapermaga.sco.system.ScoPlugin;
import eu.reapermaga.sco.system.command.SubCommand;
import eu.reapermaga.sco.system.user.SpigotUser;
import org.bukkit.entity.Player;

/**
 * Copyright (c) ReaperMaga, All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by ReaperMaga
 **/
public class ReloadCommand extends SubCommand<Player> {

    public ReloadCommand() {
        super("reload", "rl");
    }

    @Override
    public boolean onExecute(Player player, String[] args) {
        SpigotUser user = SpigotUser.getSpigotUser(player);
        ScoPlugin.getInstance().getConfigurationFiles().getConfigFile().write();
        user.sendMessage("§7You §asuccessfully §7reloaded the configuration files.");
        return true;
    }
}
