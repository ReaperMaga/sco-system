package eu.reapermaga.sco.system.command.impl;

import eu.reapermaga.sco.system.command.Commander;
import eu.reapermaga.sco.system.command.annon.Senders;
import eu.reapermaga.sco.system.io.Message;
import eu.reapermaga.sco.system.user.SpigotUser;
import eu.reapermaga.sco.system.user.obj.Settings;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

/**
 * Copyright (c) ReaperMaga, All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by ReaperMaga
 **/
@Senders(senders = Player.class)
public class ReplyCommand extends Commander<Player> {

    public ReplyCommand() {
        super("reply", "r");
    }

    @Override
    public void send(Player player, String[] args) {


        if(args.length >= 1){
            SpigotUser user = SpigotUser.getSpigotUser(player);
            if(!user.containsTag(MessageCommand.REPLY_KEY)){
                user.sendMessage("§cYou have nobody to write back.");
                return;
            }
            String name = user.getTagValue(MessageCommand.REPLY_KEY);
            Player target = Bukkit.getPlayer(name);
            if(target == null){
                player.sendMessage(Message.NOT_ONLINE);
                return;
            }
            if(!target.isOnline()){
                player.sendMessage(Message.NOT_ONLINE);
                return;
            }
            SpigotUser targetUser = SpigotUser.getSpigotUser(target);
            if(!targetUser.isSetting(Settings.PRIVATE_MESSAGES)){
                user.sendMessage("§cThis player has disabled his private messages.");
                return;
            }


            StringBuilder message = new StringBuilder();
            for (int i = 0; i < args.length; i++) {
                message.append(" ").append(args[i]);
            }
            final String text =  "§7" + message.toString().substring(1).replace("&", "§");
            user.sendMessage("§8[§a" + player.getName() + "§8] §8➥ §8[§7" + target.getName() + "§8] " + Message.ARROW + text);
            targetUser.sendMessage("§8[§7" + player.getName() + "§8] §8➥ §8[§a" + target.getName() + "§8] " + Message.ARROW + text);

            targetUser.setTag(MessageCommand.REPLY_KEY, player.getName());
            return;
        }
        player.sendMessage(Message.PREFIX + "§cUsage: /reply <Message>");
    }
}
