package eu.reapermaga.sco.system.command.impl.outpost.sub;

import eu.reapermaga.sco.system.ScoPlugin;
import eu.reapermaga.sco.system.command.SubCommand;
import eu.reapermaga.sco.system.outpost.OutpostHandler;
import eu.reapermaga.sco.system.outpost.obj.Outpost;
import eu.reapermaga.sco.system.user.SpigotUser;
import org.bukkit.Location;
import org.bukkit.entity.Player;

/**
 * Copyright (c) ReaperMaga, All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by ReaperMaga
 **/
public class DeleteSubCommand extends SubCommand<Player> {

    public DeleteSubCommand() {
        super("delete");
    }

    @Override
    public boolean onExecute(Player player, String[] args) {
        SpigotUser user = SpigotUser.getSpigotUser(player);
        OutpostHandler handler = ScoPlugin.getInstance().getOutpostHandler();
        if(args.length >= 1){
            String name = args[1];
            if(!handler.existsOutpostByName(name)) {
                user.sendMessage("§cThis Outpost doesn't exists.");
                return true;
            }
            Outpost outpost = handler.getOutpostByName(name);
            handler.deleteOutpost(outpost);
            user.sendMessage("§7You deleted the Outpost §e" + outpost.getName() +"§7.");
            return true;
        }
        return false;
    }
}
