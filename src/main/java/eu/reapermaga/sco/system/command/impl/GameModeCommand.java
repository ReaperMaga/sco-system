package eu.reapermaga.sco.system.command.impl;


import eu.reapermaga.sco.system.command.Commander;
import eu.reapermaga.sco.system.command.annon.Permission;
import eu.reapermaga.sco.system.command.annon.Senders;
import eu.reapermaga.sco.system.io.Message;
import eu.reapermaga.sco.system.user.SpigotUser;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.entity.Player;

/**
 * Copyright (c) ReaperMaga, All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by ReaperMaga
 **/
@Senders(senders = Player.class)
@Permission(permission = Message.PERMISSION_PREFIX + "gamemode")
public class GameModeCommand extends Commander<Player> {

    public GameModeCommand() {
        super("gamemode", "gm");
    }

    @Override
    public void send(Player player, String[] args) {
        SpigotUser spigotUser = SpigotUser.getSpigotUser(player);
        if(args.length == 1) {
            String gamemode = args[0];
            if(gamemode.equalsIgnoreCase("survival")
                    || gamemode.equalsIgnoreCase("s") || gamemode.equals("0")) {
                changeGameMode(spigotUser, GameMode.SURVIVAL);
            } else if(gamemode.equalsIgnoreCase("creative")
                    || gamemode.equalsIgnoreCase("c") || gamemode.equals("1")) {
                changeGameMode(spigotUser, GameMode.CREATIVE);
            } else if(gamemode.equalsIgnoreCase("adventure")
                    || gamemode.equalsIgnoreCase("2") || gamemode.equals("2")) {
                changeGameMode(spigotUser, GameMode.ADVENTURE);
            } else if(gamemode.equalsIgnoreCase("spectator")
                    || gamemode.equalsIgnoreCase("spec") || gamemode.equals("3")) {
                changeGameMode(spigotUser, GameMode.SPECTATOR);
            } else {
                sendHelp(spigotUser);
            }
        } else if(args.length >= 2) {
            String gamemode = args[0];
            String playerName = args[1];
            Player target = Bukkit.getPlayer(playerName);
            if(target == null){
                player.sendMessage(Message.NOT_ONLINE);
                return;
            }
            if(gamemode.equalsIgnoreCase("survival")
                    || gamemode.equalsIgnoreCase("s") || gamemode.equals("0")) {
                changeGameModeTarget(spigotUser, target, GameMode.SURVIVAL);
            } else if(gamemode.equalsIgnoreCase("creative")
                    || gamemode.equalsIgnoreCase("c") || gamemode.equals("1")) {
                changeGameModeTarget(spigotUser, target, GameMode.CREATIVE);
            } else if(gamemode.equalsIgnoreCase("adventure")
                    || gamemode.equalsIgnoreCase("2") || gamemode.equals("2")) {
                changeGameModeTarget(spigotUser, target, GameMode.ADVENTURE);
            } else if(gamemode.equalsIgnoreCase("spectator")
                    || gamemode.equalsIgnoreCase("spec") || gamemode.equals("3")) {
                changeGameModeTarget(spigotUser, target, GameMode.SPECTATOR);
            } else {
                sendHelp(spigotUser);
            }
        } else {
            sendHelp(spigotUser);
        }
    }

    private void sendHelp(SpigotUser user) {
        user.sendMessage("§cUsage: /gamemode <0, 1, 2, 3> <Player>");
    }

    private void changeGameMode(SpigotUser user, GameMode mode){
        user.sendMessage("§7You are now in GameMode §a" + mode.name() + "§7.");
        user.getPlayer().setGameMode(mode);
    }

    private void changeGameModeTarget(SpigotUser user, Player target, GameMode mode){
        user.sendMessage("§7The player §b" + target.getName() + " §7is now in GameMode §a" + mode.name() + "§7.");
        target.sendMessage(Message.PREFIX + "§7The player §b" + user.getPlayer().getName()
                + " §7changed your GameMode in§a" + mode.name() + ".");
        target.setGameMode(mode);
    }
}
