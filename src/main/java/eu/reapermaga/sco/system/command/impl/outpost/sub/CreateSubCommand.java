package eu.reapermaga.sco.system.command.impl.outpost.sub;

import eu.reapermaga.sco.system.ScoPlugin;
import eu.reapermaga.sco.system.command.SubCommand;
import eu.reapermaga.sco.system.outpost.OutpostHandler;
import eu.reapermaga.sco.system.outpost.obj.Outpost;
import eu.reapermaga.sco.system.user.SpigotUser;
import org.bukkit.Location;
import org.bukkit.entity.Player;

/**
 * Copyright (c) ReaperMaga, All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by ReaperMaga
 **/
public class CreateSubCommand extends SubCommand<Player> {

    public CreateSubCommand() {
        super("create");
    }

    @Override
    public boolean onExecute(Player player, String[] args) {
        SpigotUser user = SpigotUser.getSpigotUser(player);
        OutpostHandler handler = ScoPlugin.getInstance().getOutpostHandler();
        if(args.length >= 1){
            String name = args[1];
            if(handler.existsOutpostByName(name)) {
                user.sendMessage("§cThis Outpost already exists.");
                return true;
            }
            if(!user.containsTag(OutpostHandler.LOCATIOM_A_KEY) || !user.containsTag(OutpostHandler.LOCATIOM_B_KEY)) {
                user.sendMessage("§cYou need to mark 2 locations. /outpost wand");
                return true;
            }
            Location locationA = user.getTagValue(OutpostHandler.LOCATIOM_A_KEY);
            Location locationB = user.getTagValue(OutpostHandler.LOCATIOM_B_KEY);
            Location center = player.getLocation();

            handler.createOutpost(name, center, locationA, locationB);
            user.sendMessage("§7You successfully created the Outpost §a" + name + "§7.");
            return true;
        }
        return false;
    }
}
