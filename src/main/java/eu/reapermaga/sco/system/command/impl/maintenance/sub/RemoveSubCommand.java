package eu.reapermaga.sco.system.command.impl.maintenance.sub;

import eu.reapermaga.sco.system.ScoPlugin;
import eu.reapermaga.sco.system.command.SubCommand;
import eu.reapermaga.sco.system.file.yml.impl.MaintenanceFile;
import eu.reapermaga.sco.system.user.SpigotUser;
import org.bukkit.entity.Player;

/**
 * Copyright (c) ReaperMaga, All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by ReaperMaga
 **/
public class RemoveSubCommand extends SubCommand<Player> {

    public RemoveSubCommand() {
        super("remove", "delete");
    }

    @Override
    public boolean onExecute(Player player, String[] args) {
        SpigotUser user = SpigotUser.getSpigotUser(player);
        MaintenanceFile file = ScoPlugin.getInstance().getConfigurationFiles().getMaintenanceFile();
        if(args.length >= 2) {
            String name = args[1];
            if(!file.existsPlayer(name)){
                user.sendMessage("§cThis player is not in the list.");
                return true;
            }
            user.sendMessage("§c" + name + " §7has been removed from the maintenance list.");
            file.removePlayer(file.getPlayerByName(name));
            return true;
        }
        return false;
    }
}
