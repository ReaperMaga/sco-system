package eu.reapermaga.sco.system.command.impl;

import eu.reapermaga.sco.system.command.Commander;
import eu.reapermaga.sco.system.command.annon.Permission;
import eu.reapermaga.sco.system.command.annon.Senders;
import eu.reapermaga.sco.system.io.Message;
import eu.reapermaga.sco.system.user.SpigotUser;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

/**
 * Copyright (c) ReaperMaga, All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by ReaperMaga
 **/
@Senders(senders = Player.class)
@Permission(permission = Message.PERMISSION_PREFIX + "food")
public class FoodCommand extends Commander<Player> {

    public FoodCommand() {
        super("food", "eat", "essen");
    }

    @Override
    public void send(Player player, String[] args) {
        SpigotUser user = SpigotUser.getSpigotUser(player);
        if(args.length == 0) {
            user.sendMessage("§7Your §aappetite §7has been sated.");
            player.setFoodLevel(20);
        } else {
            String name = args[0];
            Player target = Bukkit.getPlayer(name);
            if(target == null){
                player.sendMessage(Message.NOT_ONLINE);
                return;
            }
            user.sendMessage("§a" + target.getName() + "'s appetite has been sated.");
            target.sendMessage(Message.PREFIX + "§a" + player.getName() + " §7has sated your appetite.");
            target.setFoodLevel(20);
        }
    }


}
