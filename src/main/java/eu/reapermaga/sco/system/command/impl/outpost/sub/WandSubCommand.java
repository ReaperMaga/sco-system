package eu.reapermaga.sco.system.command.impl.outpost.sub;

import com.google.common.base.Joiner;
import eu.reapermaga.sco.system.ScoPlugin;
import eu.reapermaga.sco.system.command.SubCommand;
import eu.reapermaga.sco.system.outpost.OutpostHandler;
import eu.reapermaga.sco.system.user.SpigotUser;
import eu.reapermaga.sco.system.util.SpigotUtil;
import org.bukkit.entity.Player;

/**
 * Copyright (c) ReaperMaga, All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by ReaperMaga
 **/
public class WandSubCommand extends SubCommand<Player> {

    public WandSubCommand() {
        super("wand");
    }

    @Override
    public boolean onExecute(Player player, String[] args) {
        SpigotUser user = SpigotUser.getSpigotUser(player);
        SpigotUtil.addItem(player, OutpostHandler.REGION_WAND);
        user.sendMessage("§7You received the §dOutpost Wand§7.");
        return true;
    }
}
