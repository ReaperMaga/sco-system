package eu.reapermaga.sco.system.command.impl;

import eu.reapermaga.sco.system.command.Commander;
import eu.reapermaga.sco.system.command.annon.Senders;
import eu.reapermaga.sco.system.io.Message;
import eu.reapermaga.sco.system.user.SpigotUser;
import eu.reapermaga.sco.system.user.obj.Settings;
import org.bukkit.Bukkit;
import org.bukkit.Sound;
import org.bukkit.entity.Player;

/**
 * Copyright (c) ReaperMaga, All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by ReaperMaga
 **/
@Senders(senders = Player.class)
public class MessageCommand extends Commander<Player> {

    public static final String REPLY_KEY = "reply";

    public MessageCommand() {
        super("message", "msg", "tell", "minecraft:tell");
    }

    @Override
    public void send(Player player, String[] args) {
        SpigotUser user = SpigotUser.getSpigotUser(player);
        if(args.length >= 2){
            String name = args[0];
            Player target = Bukkit.getPlayer(name);
            if(target == null){
                player.sendMessage(Message.NOT_ONLINE);
                return;
            }
            if(target.getName().equals(player.getName())) {
                user.sendMessage("§cWhy would you do that?");
                return;
            }
            SpigotUser targetUser = SpigotUser.getSpigotUser(target);
            if(!targetUser.isSetting(Settings.PRIVATE_MESSAGES)){
                user.sendMessage("§cThis player has disabled his private messages.");
                return;
            }

            targetUser.setTag(REPLY_KEY, player.getName());

            StringBuilder message = new StringBuilder();
            for (int i = 1; i < args.length; i++) {
                message.append(" ").append(args[i]);
            }
            final String text = "§7" + message.toString().substring(1);
            user.sendMessage("§8[§a" + player.getName() + "§8] §8➥ §8[§7" + target.getName() + "§8] " + Message.ARROW + text);
            targetUser.sendMessage("§8[§7" + player.getName() + "§8] §8➥ §8[§a" + target.getName() + "§8] " + Message.ARROW + text);
            targetUser.playSound(Sound.ENTITY_ITEM_PICKUP);
            return;
        }
        user.sendMessage("§cUsage: /msg <Player> <Message>");
    }


}
