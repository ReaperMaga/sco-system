package eu.reapermaga.sco.system.command.impl;

import eu.reapermaga.sco.system.ScoPlugin;
import eu.reapermaga.sco.system.command.Commander;
import eu.reapermaga.sco.system.command.annon.Permission;
import eu.reapermaga.sco.system.command.annon.Senders;
import eu.reapermaga.sco.system.io.Message;
import eu.reapermaga.sco.system.location.LocationHandler;
import eu.reapermaga.sco.system.location.WarpConstants;
import eu.reapermaga.sco.system.user.SpigotUser;
import org.bukkit.entity.Player;

/**
 * Copyright (c) ReaperMaga, All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by ReaperMaga
 **/
@Senders(senders = Player.class)
@Permission(permission = Message.PERMISSION_PREFIX + "spawn")
public class SpawnCommand extends Commander<Player> {

    public SpawnCommand() {
        super("spawn");
    }

    @Override
    public void send(Player player, String[] args) {
        SpigotUser user = SpigotUser.getSpigotUser(player);
        LocationHandler handler = ScoPlugin.getInstance().getLocationHandler();
        if(!handler.existsLocationByName(WarpConstants.SPAWN.name())) {
            user.sendMessage("§cSpawn has not yet been set.");
            return;
        }
        player.teleport(handler.getLocationByName(WarpConstants.SPAWN.name()));
        user.sendMessage("§7Teleported to §aspawn§7.");
    }


}
