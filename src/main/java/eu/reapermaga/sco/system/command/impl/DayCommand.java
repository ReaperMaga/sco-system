package eu.reapermaga.sco.system.command.impl;

import eu.reapermaga.sco.system.command.Commander;
import eu.reapermaga.sco.system.command.annon.Permission;
import eu.reapermaga.sco.system.command.annon.Senders;
import eu.reapermaga.sco.system.io.Message;
import eu.reapermaga.sco.system.user.SpigotUser;
import org.bukkit.World;
import org.bukkit.entity.Player;

/**
 * Copyright (c) ReaperMaga, All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by ReaperMaga
 **/
@Senders(senders = Player.class)
@Permission(permission = Message.PERMISSION_PREFIX + "day")
public class DayCommand extends Commander<Player> {

    public DayCommand() {
        super("day", "tag", "d");
    }

    @Override
    public void send(Player player, String[] args) {
        SpigotUser user = SpigotUser.getSpigotUser(player);
        World world = player.getWorld();
        world.setTime(0);
        world.setThunderDuration(0);
        world.setThundering(false);
        world.setStorm(false);
        user.sendMessage("§7The weather has been §acleared §7and time has been set to §aday§7.");
    }


}
