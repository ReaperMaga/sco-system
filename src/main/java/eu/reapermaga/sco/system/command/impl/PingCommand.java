package eu.reapermaga.sco.system.command.impl;

import eu.reapermaga.sco.system.command.Commander;
import eu.reapermaga.sco.system.command.annon.Senders;
import eu.reapermaga.sco.system.io.Message;
import eu.reapermaga.sco.system.user.SpigotUser;
import org.bukkit.Bukkit;
import org.bukkit.craftbukkit.v1_12_R1.entity.CraftPlayer;
import org.bukkit.entity.Player;

/**
 * Copyright (c) ReaperMaga, All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by ReaperMaga
 **/
@Senders(senders = Player.class)
public class PingCommand extends Commander<Player> {

    public PingCommand() {
        super("ping");
    }

    @Override
    public void send(Player player, String[] args) {
        SpigotUser user = SpigotUser.getSpigotUser(player);
        if(args.length == 0) {
            int playerping = ((CraftPlayer) player).getHandle().ping;
            user.sendMessage("§aYour ping§8: §7" + playerping + "ms");
        } else {
            String name = args[0];
            Player target = Bukkit.getPlayer(name);
            if(target == null){
                player.sendMessage(Message.NOT_ONLINE);
                return;
            }
            int playerping = ((CraftPlayer) target).getHandle().ping;
            user.sendMessage("§aPing of " + target.getName() + "§8: §7" + playerping + "ms");
        }
    }


}
