package eu.reapermaga.sco.system.command.impl;

import eu.reapermaga.sco.system.command.Commander;
import eu.reapermaga.sco.system.command.annon.Permission;
import eu.reapermaga.sco.system.command.annon.Senders;
import eu.reapermaga.sco.system.io.Message;
import eu.reapermaga.sco.system.user.SpigotUser;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

/**
 * Copyright (c) ReaperMaga, All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by ReaperMaga
 **/
@Senders(senders = Player.class)
@Permission(permission = Message.PERMISSION_PREFIX + "fly")
public class FlyCommand extends Commander<Player> {

    public FlyCommand() {
        super("fly");
    }

    @Override
    public void send(Player player, String[] args) {
        SpigotUser spigotUser = SpigotUser.getSpigotUser(player);
        if(args.length >= 1) {
            String playerName = args[0];
            Player target = Bukkit.getPlayer(playerName);
            if(target == null){
                player.sendMessage(Message.NOT_ONLINE);
                return;
            }
            if(!target.getAllowFlight()){
                target.setAllowFlight(true);
                target.sendMessage(Message.PREFIX + "§a" + player.getName() + " §7enabled you to fly.");
                spigotUser.sendMessage("§a" + target.getName() + " §7flight has been enabled.");
            } else {
                spigotUser.sendMessage("§c" + target.getName() + " §7flight has been disabled.");
                target.setAllowFlight(false);
                target.sendMessage(Message.PREFIX + "§c" + player.getName() + " §7disabled your flight.");
            }
        } else {
            if(!player.getAllowFlight()){
                player.setAllowFlight(true);
                spigotUser.sendMessage("§aFlight §7has been enabled.");
            } else {
                player.setAllowFlight(false);
                spigotUser.sendMessage("§cFlight §7has been disabled.");
            }
        }
    }


}
