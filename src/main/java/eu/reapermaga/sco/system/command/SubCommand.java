package eu.reapermaga.sco.system.command;

import lombok.Getter;

import java.util.Arrays;
import java.util.List;

/**
 * Copyright (c) ReaperMaga, All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by ReaperMaga, 13.07.2018
 **/
@Getter
public abstract class SubCommand<K> {

    /** Fields **/
    private final String name;
    private final List<String> aliases;

    /**
     * Constructor
     * @param name
     * @param aliases
     */
    public SubCommand(String name, String... aliases) {
        this.name = name;
        this.aliases = Arrays.asList(aliases);
    }

    /**
     * Check if value is equals command or alias
     * @param value
     * @return
     */
    public boolean check(String value){
        if(name.equalsIgnoreCase(value)){
            return true;
        }
        for(String alias : getAliases()){
            if(alias.equalsIgnoreCase(value)){
                return true;
            }
        }
        return false;
    }

    /**
     * Abstract Method
     * execute command
     **/
    public abstract boolean onExecute(K sender, String[] args);
}
