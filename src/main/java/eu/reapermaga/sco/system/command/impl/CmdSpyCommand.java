package eu.reapermaga.sco.system.command.impl;

import eu.reapermaga.sco.system.command.Commander;
import eu.reapermaga.sco.system.command.annon.Permission;
import eu.reapermaga.sco.system.command.annon.Senders;
import eu.reapermaga.sco.system.io.Message;
import eu.reapermaga.sco.system.user.SpigotUser;
import org.bukkit.entity.Player;

/**
 * Copyright (c) ReaperMaga, All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by ReaperMaga
 **/
@Senders(senders = Player.class)
@Permission(permission = Message.PERMISSION_PREFIX + "cmdspy")
public class CmdSpyCommand extends Commander<Player> {

    public static final String SPY_KEY = "cmdSpy";

    public CmdSpyCommand() {
        super("cmdspy", "commandspy", "spycmd");
    }

    @Override
    public void send(Player player, String[] args) {
        SpigotUser user = SpigotUser.getSpigotUser(player);
        if(!user.containsTag(SPY_KEY)){
            user.setTag(SPY_KEY, true);
            user.sendMessage("§aCommand-Spy §7activated.");
        } else {
            user.removeTag(SPY_KEY);
            user.sendMessage("§cCommand-Spy §7deactivated.");
        }

    }


}
