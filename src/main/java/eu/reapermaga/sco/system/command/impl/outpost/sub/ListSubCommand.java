package eu.reapermaga.sco.system.command.impl.outpost.sub;

import com.google.common.base.Joiner;
import eu.reapermaga.sco.system.ScoPlugin;
import eu.reapermaga.sco.system.command.SubCommand;
import eu.reapermaga.sco.system.outpost.OutpostHandler;
import eu.reapermaga.sco.system.outpost.obj.Outpost;
import eu.reapermaga.sco.system.user.SpigotUser;
import org.bukkit.entity.Player;

/**
 * Copyright (c) ReaperMaga, All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by ReaperMaga
 **/
public class ListSubCommand extends SubCommand<Player> {

    public ListSubCommand() {
        super("list");
    }

    @Override
    public boolean onExecute(Player player, String[] args) {
        SpigotUser user = SpigotUser.getSpigotUser(player);
        OutpostHandler handler = ScoPlugin.getInstance().getOutpostHandler();
        user.sendMessage("§dOutposts§8: §7" + Joiner.on("§8, §7").join(handler.getEntity()));
        return true;
    }
}
