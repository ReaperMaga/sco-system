package eu.reapermaga.sco.system.command.impl;

import eu.reapermaga.sco.system.command.Commander;
import eu.reapermaga.sco.system.command.annon.Permission;
import eu.reapermaga.sco.system.command.annon.Senders;
import eu.reapermaga.sco.system.io.Message;
import eu.reapermaga.sco.system.user.inventory.SettingsInventory;
import org.bukkit.Sound;
import org.bukkit.entity.Player;

/**
 * Copyright (c) ReaperMaga, All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by ReaperMaga
 **/
@Senders(senders = Player.class)
public class SettingsCommand extends Commander<Player> {

    public SettingsCommand() {
        super("settings", "setting");
    }

    @Override
    public void send(Player player, String[] args) {
        SettingsInventory settingsInventory = new SettingsInventory(player);
        settingsInventory.openInventory(player, Sound.BLOCK_WOOD_BUTTON_CLICK_ON);
    }
}
