package eu.reapermaga.sco.system.command.impl;

import eu.reapermaga.sco.system.command.Commander;
import eu.reapermaga.sco.system.command.annon.Permission;
import eu.reapermaga.sco.system.command.annon.Senders;
import eu.reapermaga.sco.system.io.Message;
import eu.reapermaga.sco.system.user.SpigotUser;
import eu.reapermaga.sco.system.util.SpigotUtil;
import eu.reapermaga.sco.system.util.inventory.util.ItemCreator;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

/**
 * Copyright (c) ReaperMaga, All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by ReaperMaga
 **/
@Senders(senders = Player.class)
@Permission(permission = Message.PERMISSION_PREFIX + "skull")
public class SkullCommand extends Commander<Player> {

    public SkullCommand() {
        super("head");
    }

    @Override
    public void send(Player player, String[] args) {
        SpigotUser user = SpigotUser.getSpigotUser(player);
        if(args.length == 0) {
            user.sendMessage("§7You §areceived §7your head.");
            addSkull(player, player.getName());
        } else {
            String name = args[0];
            user.sendMessage("§7You received §a"+name+" §7head.");
            addSkull(player, name);
        }
    }

    private void addSkull(Player player, String name) {
        SpigotUtil.addItem(player, new ItemCreator().setMaterial(Material.SKULL_ITEM).setData(3).setSkull(name).build());
    }


}
