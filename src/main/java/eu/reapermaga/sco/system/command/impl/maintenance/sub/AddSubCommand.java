package eu.reapermaga.sco.system.command.impl.maintenance.sub;

import eu.reapermaga.sco.system.ScoPlugin;
import eu.reapermaga.sco.system.command.SubCommand;
import eu.reapermaga.sco.system.file.yml.impl.MaintenanceFile;
import eu.reapermaga.sco.system.user.SpigotUser;
import org.bukkit.entity.Player;

/**
 * Copyright (c) ReaperMaga, All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by ReaperMaga
 **/
public class AddSubCommand extends SubCommand<Player> {

    public AddSubCommand() {
        super("add", "create");
    }

    @Override
    public boolean onExecute(Player player, String[] args) {
        SpigotUser user = SpigotUser.getSpigotUser(player);
        MaintenanceFile file = ScoPlugin.getInstance().getConfigurationFiles().getMaintenanceFile();
        if(args.length >= 2) {
            String name = args[1];
            if(file.existsPlayer(name)){
                user.sendMessage("§cThis player is already in the list.");
                return true;
            }
            user.sendMessage("§a" + name + " §7has been added to maintenance list.");
            file.addPlayer(name);
            return true;
        }
        return false;
    }
}
