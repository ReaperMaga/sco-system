package eu.reapermaga.sco.system.command.impl.maintenance.sub;

import eu.reapermaga.sco.system.ScoPlugin;
import eu.reapermaga.sco.system.command.SubCommand;
import eu.reapermaga.sco.system.file.yml.impl.MaintenanceFile;
import eu.reapermaga.sco.system.user.SpigotUser;
import org.bukkit.entity.Player;

/**
 * Copyright (c) ReaperMaga, All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by ReaperMaga
 **/
public class ClearSubCommand extends SubCommand<Player> {

    public ClearSubCommand() {
        super("clear");
    }

    @Override
    public boolean onExecute(Player player, String[] args) {
        SpigotUser user = SpigotUser.getSpigotUser(player);
        MaintenanceFile file = ScoPlugin.getInstance().getConfigurationFiles().getMaintenanceFile();
        file.clearPlayers();
        user.sendMessage("§7You cleared the §amaintenance list§7.");
        return true;
    }
}
