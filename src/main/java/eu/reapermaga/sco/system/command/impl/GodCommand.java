package eu.reapermaga.sco.system.command.impl;

import eu.reapermaga.sco.system.command.Commander;
import eu.reapermaga.sco.system.command.annon.Permission;
import eu.reapermaga.sco.system.command.annon.Senders;
import eu.reapermaga.sco.system.io.Message;
import eu.reapermaga.sco.system.user.SpigotUser;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

/**
 * Copyright (c) ReaperMaga, All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by ReaperMaga
 **/
@Senders(senders = Player.class)
@Permission(permission = Message.PERMISSION_PREFIX + "god")
public class GodCommand extends Commander<Player> {

    public static final String GOD_KEY = "god";

    public GodCommand() {
        super("god");
    }

    @Override
    public void send(Player player, String[] args) {
        SpigotUser user = SpigotUser.getSpigotUser(player);
        if(args.length == 0) {
            if(!user.containsTag(GOD_KEY)) {
                user.sendMessage("§aGod Mode §7has been §aenabled§7.");
                user.setTag(GOD_KEY, true);
            } else {
                user.sendMessage("§cGod Mode §7has been §cdisabled§7.");
                user.removeTag(GOD_KEY);
            }
        } else {
            String name = args[0];
            Player target = Bukkit.getPlayer(name);
            if(target == null){
                player.sendMessage(Message.NOT_ONLINE);
                return;
            }
            SpigotUser targetUser = SpigotUser.getSpigotUser(target);
            if(!targetUser.containsTag(GOD_KEY)) {
                user.sendMessage("§a" + target.getName() + " §7is now in §aGod Mode§7.");
                targetUser.sendMessage("§a" + player.getName() + " §7has §aenabled §7your §aGod Mode§7.");
                targetUser.setTag(GOD_KEY, true);
            } else {
                user.sendMessage("§c" + target.getName() + " §7is no longer in §cGod Mode§7.");
                targetUser.sendMessage("§c" + player.getName() + " §7has §cdisabled §7your §aGod Mode§7.");
                targetUser.removeTag(GOD_KEY);
            }
        }
    }


}
