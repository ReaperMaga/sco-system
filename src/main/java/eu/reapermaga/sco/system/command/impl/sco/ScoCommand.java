package eu.reapermaga.sco.system.command.impl.sco;

import eu.reapermaga.sco.system.command.Commander;
import eu.reapermaga.sco.system.command.annon.Permission;
import eu.reapermaga.sco.system.command.annon.Senders;
import eu.reapermaga.sco.system.command.impl.sco.sub.ChangeGrassRadiusCommand;
import eu.reapermaga.sco.system.command.impl.sco.sub.ReloadCommand;
import eu.reapermaga.sco.system.io.Message;
import org.bukkit.entity.Player;

/**
 * Copyright (c) ReaperMaga, All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by ReaperMaga
 **/
@Senders(senders = Player.class)
@Permission(permission = Message.PERMISSION_PREFIX + "main")
public class ScoCommand extends Commander<Player> {

    public ScoCommand() {
        super("sco", "swordcraftonline");
        getSubCommands().add(new ReloadCommand());
        getSubCommands().add(new ChangeGrassRadiusCommand());
    }

    @Override
    public void send(Player player, String[] args) {
        if(args.length >= 1){
            boolean sub = subCommand(player, args);
            if(!sub){
                sendHelp(player);
            }
        } else {
            sendHelp(player);
        }
    }

    private void sendHelp(Player player) {
        player.sendMessage(Message.UPTEXT_FIRST + "§aHelp" + Message.UPTEXT_SECOND);
        player.sendMessage("");
        player.sendMessage(Message.ARROW + "§aSCO - Usage");
        player.sendMessage("");
        player.sendMessage(Message.ARROW + "§8/§asco changeGrassRadius <Radius> §8" + Message.LINE + " §7Change the grass remove radius");
        player.sendMessage(Message.ARROW + "§8/§asco reload §8" + Message.LINE + " §7Reload all files");
        player.sendMessage("");
        player.sendMessage(Message.DOWNTEXT_FIRST + "§aHelp" + Message.DOWNTEXT_SECOND);
    }
}
