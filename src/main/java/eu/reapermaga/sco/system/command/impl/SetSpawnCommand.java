package eu.reapermaga.sco.system.command.impl;

import eu.reapermaga.sco.system.ScoPlugin;
import eu.reapermaga.sco.system.command.Commander;
import eu.reapermaga.sco.system.command.annon.Permission;
import eu.reapermaga.sco.system.command.annon.Senders;
import eu.reapermaga.sco.system.io.Message;
import eu.reapermaga.sco.system.location.LocationHandler;
import eu.reapermaga.sco.system.location.WarpConstants;
import eu.reapermaga.sco.system.user.SpigotUser;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

/**
 * Copyright (c) ReaperMaga, All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by ReaperMaga
 **/
@Senders(senders = Player.class)
@Permission(permission = Message.PERMISSION_PREFIX + "setspawn")
public class SetSpawnCommand extends Commander<Player> {

    public SetSpawnCommand() {
        super("setspawn");
    }

    @Override
    public void send(Player player, String[] args) {
        SpigotUser user = SpigotUser.getSpigotUser(player);
        LocationHandler handler = ScoPlugin.getInstance().getLocationHandler();
        handler.createLocation(WarpConstants.SPAWN.name(), player.getLocation());
        user.sendMessage("§7You §asuccessfully §7set spawn.");
    }


}
