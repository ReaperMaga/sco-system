package eu.reapermaga.sco.system.command.impl;

import eu.reapermaga.sco.system.ScoPlugin;
import eu.reapermaga.sco.system.command.Commander;
import eu.reapermaga.sco.system.command.annon.Permission;
import eu.reapermaga.sco.system.command.annon.Senders;
import eu.reapermaga.sco.system.file.yml.impl.ConfigFile;
import eu.reapermaga.sco.system.io.Message;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

/**
 * Copyright (c) ReaperMaga, All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by ReaperMaga
 **/
@Senders(senders = Player.class)
@Permission(permission = Message.PERMISSION_PREFIX + "globalmute")
public class GlobalMuteCommand extends Commander<Player> {

    public GlobalMuteCommand() {
        super("globalmute", "globalm");
    }

    @Override
    public void send(Player player, String[] args) {
        ConfigFile file = ScoPlugin.getInstance().getConfigurationFiles().getConfigFile();
        if(!file.isGlobalmute()){
            Bukkit.broadcastMessage(Message.PREFIX + "The GlobalMute has been §aactivated§7.");
            file.setGlobalmute(true);
        } else {
            Bukkit.broadcastMessage(Message.PREFIX + "The GlobalMute has been §cdeactivated§7.");
            file.setGlobalmute(false);
        }
    }
}
