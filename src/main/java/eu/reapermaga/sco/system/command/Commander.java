package eu.reapermaga.sco.system.command;


import com.google.common.collect.Lists;
import eu.reapermaga.sco.system.command.annon.Permission;
import eu.reapermaga.sco.system.command.annon.Senders;
import eu.reapermaga.sco.system.io.Message;
import lombok.Getter;
import lombok.Setter;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

import java.util.Arrays;
import java.util.List;

/**
 * Copyright (c) ReaperMaga, All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by ReaperMaga, 12.07.2018
 **/
public abstract class Commander<K> extends Command {

    /**Annontation**/
    private Senders senders;
    private Permission permission;
    @Getter @Setter private List<SubCommand> subCommands;

    /**
     * Constructor
     * @param name = command name
     * @param aliases = command aliases
     */
    public Commander(String name, String... aliases) {
        super(name);
        setAliases(Arrays.asList(aliases));
        this.subCommands = Lists.newLinkedList();
        Class<?> clazz = this.getClass();
        if(this.getClass().isAnnotationPresent(Senders.class)){
            senders = clazz.getAnnotation(Senders.class);
        }
        if(this.getClass().isAnnotationPresent(Permission.class)){
            permission = clazz.getAnnotation(Permission.class);
        }

    }

    /**
     * Default bukkit method
     * @param sender
     * @param label
     * @param args
     * @return
     */
    @Override
    public boolean execute(CommandSender sender, String label, String[] args) {
        if(senders == null){
            return true;
        }

        if(!isSender(senders.senders(), sender.getClass())){
            return true;
        }
        if(permission == null){
            send((K)sender, args);
            return true;
        }
        if(!sender.hasPermission(permission.permission())){
            sender.sendMessage(Message.NO_PERMISSION);
            return true;
        }
        send((K)sender, args);
        return true;
    }

    public  boolean subCommand(K sender, String[] args){
        for(SubCommand command : this.subCommands){
            if(command.check(args[0])) {
                return command.onExecute(sender, args);
            }
        }
        return false;
    }

    /**
     * Check if sender exists in the array
     * @param array = Class array
     * @param sender = Class sender
     * @return
     */
    private boolean isSender(Class<?>[] array, Class<?> sender){
        for(Class<?> arrayClass : array){
            if(arrayClass.getSimpleName().equals(sender.getSimpleName())){
                return true;
            }
            for(Class<?> interfaces : sender.getInterfaces()){
                if(arrayClass.getSimpleName().equals(interfaces.getSimpleName())){
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Abstract command
     * @param sender
     * @param args
     */
    public abstract void send(K sender, String[] args);


    /**
     * Gets the annontation permission class
     * @return
     */
    public Permission getAnnontationPermission() {
        return this.permission;
    }
}
