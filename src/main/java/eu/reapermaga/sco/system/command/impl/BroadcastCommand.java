package eu.reapermaga.sco.system.command.impl;

import eu.reapermaga.sco.system.command.Commander;
import eu.reapermaga.sco.system.command.annon.Permission;
import eu.reapermaga.sco.system.command.annon.Senders;
import eu.reapermaga.sco.system.io.Message;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

/**
 * Copyright (c) ReaperMaga, All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by ReaperMaga
 **/
@Senders(senders = Player.class)
@Permission(permission = Message.PERMISSION_PREFIX + "broadcast")
public class BroadcastCommand extends Commander<Player> {

    public BroadcastCommand() {
        super("broadcast", "bc");
    }

    @Override
    public void send(Player player, String[] args) {
        if(args.length >= 1){
            StringBuilder message = new StringBuilder();
            for (int i = 0; i < args.length; i++) {
                message.append(" ").append(args[i]);
            }
            final String text = message.toString().substring(1).replace("&", "§");
            Bukkit.broadcastMessage(Message.PREFIX.concat("§a").concat(text));
            return;
        }
        player.sendMessage(Message.PREFIX + "§cUsage: /broadcast <Message>");
    }
}
