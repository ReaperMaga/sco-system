package eu.reapermaga.sco.system.command.impl.outpost;

import eu.reapermaga.sco.system.ScoPlugin;
import eu.reapermaga.sco.system.command.Commander;
import eu.reapermaga.sco.system.command.annon.Permission;
import eu.reapermaga.sco.system.command.annon.Senders;
import eu.reapermaga.sco.system.command.impl.outpost.sub.*;
import eu.reapermaga.sco.system.file.yml.impl.MaintenanceFile;
import eu.reapermaga.sco.system.io.Message;
import eu.reapermaga.sco.system.user.SpigotUser;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

/**
 * Copyright (c) ReaperMaga, All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by ReaperMaga
 **/
@Senders(senders = Player.class)
@Permission(permission = Message.PERMISSION_PREFIX + "outpost")
public class OutpostCommand extends Commander<Player> {

    public OutpostCommand() {
        super("outpost");
        getSubCommands().add(new CreateSubCommand());
        getSubCommands().add(new DeleteSubCommand());
        getSubCommands().add(new TeleportSubCommand());
        getSubCommands().add(new ListSubCommand());
        getSubCommands().add(new WandSubCommand());
    }

    @Override
    public void send(Player player, String[] args) {
        if(args.length >= 1){
            boolean sub = subCommand(player, args);
            if(!sub){
                sendHelp(player);
            }
        } else {
            sendHelp(player);
        }
    }


    private void sendHelp(Player player){
        player.sendMessage(Message.UPTEXT_FIRST + "§dOutpost" + Message.UPTEXT_SECOND);
        player.sendMessage("");
        player.sendMessage(Message.ARROW + "§dOutpost - Usage");
        player.sendMessage("");
        player.sendMessage(Message.ARROW + "§8/§doutpost create <Name> §8" + Message.LINE + " §7Create an Outpost");
        player.sendMessage(Message.ARROW + "§8/§doutpost delete <Name> §8" + Message.LINE + " §7Delete an Outpost");
        player.sendMessage(Message.ARROW + "§8/§doutpost teleport <Name> §8" + Message.LINE + " §7Teleport to an Outpost");
        player.sendMessage(Message.ARROW + "§8/§doutpost list §8" + Message.LINE + " §7Show all Outposts");
        player.sendMessage(Message.ARROW + "§8/§doutpost wand §8" + Message.LINE + " §7Get the region wand");
        player.sendMessage("");
        player.sendMessage(Message.DOWNTEXT_FIRST + "§dOutpost" + Message.DOWNTEXT_SECOND);
    }
}
