package eu.reapermaga.sco.system.command.impl;

import eu.reapermaga.sco.system.ScoPlugin;
import eu.reapermaga.sco.system.command.Commander;
import eu.reapermaga.sco.system.command.annon.Permission;
import eu.reapermaga.sco.system.command.annon.Senders;
import eu.reapermaga.sco.system.io.Message;
import eu.reapermaga.sco.system.util.SpigotUtil;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

/**
 * Copyright (c) ReaperMaga, All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by ReaperMaga
 **/
@Senders(senders = Player.class)
@Permission(permission = Message.PERMISSION_PREFIX + "main")
public class TestCommand extends Commander<Player> {

    public TestCommand() {
        super("test");
    }

    @Override
    public void send(Player player, String[] args) {
        if(args.length >= 1) {
            new BukkitRunnable() {
                Location location = player.getLocation().clone().subtract(0, 1.0, 0);
                int radius = 1;
                @Override
                public void run() {
                    if(radius > 5) {
                        cancel();
                        return;
                    }
                    for(Location location : SpigotUtil.getCircleBlocksFlat(location, radius)) {
                        location.getBlock().setTypeIdAndData(Material.WOOL.getId(), (byte)5, true);
                    }

                    radius++;
                }
            }.runTaskTimer(ScoPlugin.getInstance(), 40, 40);
        } else {
            Location custom = player.getLocation().clone().subtract(0, 1.0, 0);
            for(Location location : SpigotUtil.getCircleBlocksFlat(custom, 5)) {
                location.getBlock().setType(Material.WOOL);
            }

        }

    }
}
