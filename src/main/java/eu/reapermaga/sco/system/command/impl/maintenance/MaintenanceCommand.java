package eu.reapermaga.sco.system.command.impl.maintenance;

import eu.reapermaga.sco.system.ScoPlugin;
import eu.reapermaga.sco.system.command.Commander;
import eu.reapermaga.sco.system.command.annon.Permission;
import eu.reapermaga.sco.system.command.annon.Senders;
import eu.reapermaga.sco.system.command.impl.maintenance.sub.AddSubCommand;
import eu.reapermaga.sco.system.command.impl.maintenance.sub.ClearSubCommand;
import eu.reapermaga.sco.system.command.impl.maintenance.sub.ListSubCommand;
import eu.reapermaga.sco.system.command.impl.maintenance.sub.RemoveSubCommand;
import eu.reapermaga.sco.system.file.yml.impl.MaintenanceFile;
import eu.reapermaga.sco.system.io.Message;
import eu.reapermaga.sco.system.user.SpigotUser;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

/**
 * Copyright (c) ReaperMaga, All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by ReaperMaga
 **/
@Senders(senders = Player.class)
@Permission(permission = Message.PERMISSION_PREFIX + "maintenance")
public class MaintenanceCommand extends Commander<Player> {

    public MaintenanceCommand() {
        super("maintenance", "wartung", "wartungsmodus");
        getSubCommands().add(new AddSubCommand());
        getSubCommands().add(new ClearSubCommand());
        getSubCommands().add(new ListSubCommand());
        getSubCommands().add(new RemoveSubCommand());
    }

    @Override
    public void send(Player player, String[] args) {
        SpigotUser user = SpigotUser.getSpigotUser(player);
        if(args.length == 0){
            MaintenanceFile file = ScoPlugin.getInstance().getConfigurationFiles().getMaintenanceFile();
            if(!file.isEnabled()){
                Bukkit.broadcastMessage(Message.PREFIX + "§aMaintenance §7has been enabled§7.");
                file.setEnabled(true);
                kickAllPlayer(file);
            } else {
                Bukkit.broadcastMessage(Message.PREFIX + "§cMaintenance §7has been disabled§7.");
                file.setEnabled(false);
            }
        } else {
            boolean sub = subCommand(player, args);
            if(!sub){
                sendHelp(player);
            }
        }
    }

    private void kickAllPlayer(MaintenanceFile file) {
        for(Player all : Bukkit.getOnlinePlayers()) {
            if(!file.existsPlayer(all.getName())){
                all.kickPlayer(file.getMessage());
            }
        }
    }

    private void sendHelp(Player player){
        player.sendMessage(Message.UPTEXT_FIRST + "§cMaintenance" + Message.UPTEXT_SECOND);
        player.sendMessage("");
        player.sendMessage(Message.ARROW + "§cMaintenance - Usage");
        player.sendMessage("");
        player.sendMessage(Message.ARROW + "§8/§cmaintenance §8" + Message.LINE + " §7On/Off");
        player.sendMessage(Message.ARROW + "§8/§cmaintenance add <Player> §8" + Message.LINE + " §7Add a player");
        player.sendMessage(Message.ARROW + "§8/§cmaintenance remove <Player> §8" + Message.LINE + " §7Remove a player");
        player.sendMessage(Message.ARROW + "§8/§cmaintenance clear §8" + Message.LINE + " §7Remove all players");
        player.sendMessage(Message.ARROW + "§8/§cmaintenance list §8" + Message.LINE + " §7Show all players");
        player.sendMessage("");
        player.sendMessage(Message.DOWNTEXT_FIRST + "§cMaintenance" + Message.DOWNTEXT_SECOND);
    }
}
