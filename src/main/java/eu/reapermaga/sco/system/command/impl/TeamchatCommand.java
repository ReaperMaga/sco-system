package eu.reapermaga.sco.system.command.impl;

import eu.reapermaga.sco.system.command.Commander;
import eu.reapermaga.sco.system.command.annon.Permission;
import eu.reapermaga.sco.system.command.annon.Senders;
import eu.reapermaga.sco.system.io.Message;
import eu.reapermaga.sco.system.user.SpigotUser;
import org.bukkit.Bukkit;
import org.bukkit.Sound;
import org.bukkit.entity.Player;

/**
 * Copyright (c) ReaperMaga, All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by ReaperMaga
 **/
@Senders(senders = Player.class)
@Permission(permission = Message.PERMISSION_PREFIX + "teamchat")
public class TeamchatCommand extends Commander<Player> {

    public TeamchatCommand() {
        super("teamchat", "tc");
    }

    @Override
    public void send(Player player, String[] args) {
        SpigotUser user = SpigotUser.getSpigotUser(player);
        if(args.length >= 1){
            StringBuilder message = new StringBuilder();
            for (int i = 0; i < args.length; i++) {
                message.append(" ").append(args[i]);
            }
            final String text = message.toString().substring(1);

            for(Player all : Bukkit.getOnlinePlayers()) {
                if (all.hasPermission(Message.PERMISSION_PREFIX + "teamchat")) {
                    all.sendMessage(Message.TEAMCHAT_PREFIX + player.getName() + " §8➥ §7" + text);
                    all.playSound(all.getLocation(), Sound.ENTITY_CHICKEN_EGG, 1, 1);
                }
            }
            return;
        }
        user.sendMessage("§cUsage: /teamchat <Message>");
    }


}
