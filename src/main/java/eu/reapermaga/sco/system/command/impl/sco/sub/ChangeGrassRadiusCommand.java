package eu.reapermaga.sco.system.command.impl.sco.sub;

import eu.reapermaga.sco.system.ScoPlugin;
import eu.reapermaga.sco.system.command.SubCommand;
import eu.reapermaga.sco.system.user.SpigotUser;
import eu.reapermaga.sco.system.util.SpigotUtil;
import org.bukkit.entity.Player;

/**
 * Copyright (c) ReaperMaga, All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by ReaperMaga
 **/
public class ChangeGrassRadiusCommand extends SubCommand<Player> {

    public ChangeGrassRadiusCommand() {
        super("changeGrassRadius");
    }

    @Override
    public boolean onExecute(Player player, String[] args) {
        SpigotUser user = SpigotUser.getSpigotUser(player);
        if(args.length >= 2){
            if(!SpigotUtil.isNumber(args[1])) {
                user.sendMessage("§cWrong number format.");
                return true;
            }
            int radius = Integer.valueOf(args[1]);
            ScoPlugin.getInstance().getConfigurationFiles().getConfigFile().changeGrassRadius(radius);
            user.sendMessage("§7You §asuccessfully §7changed the grass radius to §b"+radius+"§7.");
            return true;
        }
        return false;
    }
}
