package eu.reapermaga.sco.system.io;

/**
 * Copyright (c) ReaperMaga, All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by ReaperMaga
 **/
public final class Message {

    public static final String PREFIX = "§8┃ §bSCO §8● §7";
    public static final String NO_PERMISSION = PREFIX + "Sorry, you don't have enough permissions";
    public static final String NOT_ONLINE = PREFIX + "§cThis player is not online.";


    public static final String TEAMCHAT_PREFIX = "§8┃ §cTeamChat §8» §7";
    public static final String COMMAND_SPY_PREFIX = "§8┃ §cCommand-Spy §8» §7";

    public static final String PERMISSION_PREFIX = "Sco.";

    /**
     * Characters
     */
    public static String BOLD_POINT = "§8● ";
    public static String BOLD_ARROW = "§8➤ ";
    public static String ARROW = "§8» ";
    public static final String LINE =  "┃";

    /**
     * Lines
     */
    public static final String UPTEXT_FIRST = "§8▛▀▀▀▀▀▀▀▀▀▀§r ";
    public static final String UPTEXT_SECOND = " §8▀▀▀▀▀▀▀▀▀▀▜";
    public static final String DOWNTEXT_FIRST = "§8▙▄▄▄▄▄▄▄▄▄▄§r ";
    public static final String DOWNTEXT_SECOND = " §8▄▄▄▄▄▄▄▄▄▄▟";

}
