package eu.reapermaga.sco.system.location.obj;

import lombok.Getter;
import lombok.Setter;
import org.bukkit.Location;

/**
 * Copyright (c) ReaperMaga, All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by ReaperMaga
 **/
@Getter
@Setter
public class LocationWarp {

    private String name;
    private Location location;
}
