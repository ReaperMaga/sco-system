package eu.reapermaga.sco.system.location;

import com.google.common.collect.Lists;
import eu.reapermaga.sco.system.file.ConfigurationFiles;
import eu.reapermaga.sco.system.file.json.GsonConfig;
import eu.reapermaga.sco.system.location.obj.LocationWarp;
import org.bukkit.Location;

import java.util.List;

/**
 * Copyright (c) ReaperMaga, All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by ReaperMaga
 **/
public class LocationHandler extends GsonConfig<List<LocationWarp>> {

    public LocationHandler() {
        super(ConfigurationFiles.getDefaultPath(), "locations");
    }

    public void createLocation(String name, Location location){
        LocationWarp locationWarp = new LocationWarp();
        locationWarp.setName(name);
        locationWarp.setLocation(location);

        getEntity().add(locationWarp);
        save();
    }

    public Location getLocationByName(String name){
        for(LocationWarp warp : getEntity()){
            if(warp.getName().equalsIgnoreCase(name)) {
                return warp.getLocation();
            }
        }
        return null;
    }

    public boolean existsLocationByName(String name){
        for(LocationWarp warp : getEntity()){
            if(warp.getName().equalsIgnoreCase(name)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public List<LocationWarp> getDefault() {
        return Lists.newArrayList();
    }
}
